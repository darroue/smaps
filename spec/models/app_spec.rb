require 'rails_helper'
require 'support/shared_examples/models/basic_examples'

RSpec.describe App, type: :model do
  include_context 'a basic model attributes'
  it_behaves_like 'a basic model', %i(name package_name)

  it '#name_with_version returns data in correct format' do
    expect(subject.name_with_version).to eq "#{subject.name} (#{subject.version!})"
  end

  it '#version! returns version' do
    expect(subject.version!).to eq subject.version
  end

  describe "without version" do
    it '#version! returns "latest"' do
      subject[:version] = nil
      expect(subject.version!).to eq 'latest'
    end
  end
end
