require 'rails_helper'
require 'support/shared_examples/models/basic_examples'

RSpec.describe NodeGroup, type: :model do
  include_context 'a basic model attributes'
  it_behaves_like 'a basic model', %i(name)
end
