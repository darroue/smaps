require 'rails_helper'

RSpec.describe InstallController, type: :controller do
  describe 'GET #show' do
    let!(:subject) do
      Invite.create attributes_for(:invite).merge(organization: create(:organization))
    end

    describe 'with valid params' do
      it 'returns a success response' do
        get :show, params: { auth_id: subject.auth_id }
        expect(response).to be_successful
      end
    end

    describe 'with invalid params' do
      it 'returns a :not_found response' do
        get :show, params: { auth_id: '' }
        expect(response.status).to be 404 
      end

      it 'returns a :gone response for Invite that cant be used' do
        subject.use
        get :show, params: { auth_id: subject.auth_id }
        expect(response.status).to be 410
      end
    end
  end
end
