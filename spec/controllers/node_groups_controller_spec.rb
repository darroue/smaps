require 'rails_helper'
require 'support/shared_examples/controllers/crud_examples'

RSpec.describe NodeGroupsController, type: :controller do
  login_admin

  include_context 'a organization CRUD Controller'
  it_behaves_like 'a CRUD Controller'
  it_behaves_like 'a CRUD Controller with many', :nodes
  it_behaves_like 'a CRUD Controller with many', :app_groups
end
