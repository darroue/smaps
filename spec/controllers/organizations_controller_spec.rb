require 'rails_helper'
require 'support/shared_examples/controllers/crud_examples'

RSpec.describe OrganizationsController, type: :controller do
  login_admin

  include_context 'a organization CRUD Controller'
  it_behaves_like 'a CRUD Controller', custom_subject: true

  let(:default_params) do
    {}
  end

  let!(:subject) do
    subject = create :organization, valid_attributes
    subject.users << @admin

    admin_role_permissions = subject.roles.find_by_name('Owner').permissions
    admin_role = subject.roles.create name: Faker::FunnyName.name, permissions: admin_role_permissions
    admin_role.users << @admin

    subject
  end
end
