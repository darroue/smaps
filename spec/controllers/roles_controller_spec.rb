require 'rails_helper'
require 'support/shared_examples/controllers/crud_examples'

RSpec.describe RolesController, type: :controller do
  login_admin

  include_context 'a organization CRUD Controller'
  it_behaves_like 'a CRUD Controller'
  it_behaves_like 'a CRUD Controller with many', :users, 3, true
end
