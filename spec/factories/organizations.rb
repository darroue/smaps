include ::RolesHelper

FactoryBot.define do
  factory :organization do
    name { Faker::Company.name }
    after :create do |organization|
      organization.create_protected_roles
    end

    trait :invalid do
      invalid_name
    end
  end
end
