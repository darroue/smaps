FactoryBot.define do
  factory :installed_app do
    app_type
    name
    node_id
    uninstall_string
    version
    
    trait :invalid do
      invalid_name
      invalid_version
    end
  end
end
