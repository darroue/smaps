FactoryBot.define do
  user_password = Faker::Internet.password

  factory :user do
    first_name
    last_name
    full_name
    email
    active
    password
    encrypted_password
    remember_created_at
    reset_password_sent_at
    reset_password_token

    trait :invalid do
      invalid_first_name
      invalid_last_name
      invalid_email
    end
  end
end
