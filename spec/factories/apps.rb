FactoryBot.define do
  factory :app do
    name
    package_name
    version
    active
    forced

    trait :invalid do
      invalid_name
      invalid_package_name
      invalid_version
      invalid_active
      invalid_forced
    end
  end
end
