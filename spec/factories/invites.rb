FactoryBot.define do
  factory :invite do
    usages
    valid_until
    invite_type { :node }
    auth_id { nil }
    node_id { nil }
    user_id { nil }
  end
  
  trait :user_invite do
    invite_type { :user }
  end

  trait :invalid do
    invite_type { nil }
  end
end
