FactoryBot.define do
  factory :node_group do
    name
    active

    trait :invalid do
      invalid_name
      invalid_active
    end
  end
end
