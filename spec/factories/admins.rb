FactoryBot.define do
  factory :admin, class: User do
    first_name { 'Main' }
    last_name { 'Administrator' }
    email
    password
    password_confirmation

    after :create do |user|
      user.organizations << create(:organization)
      user.reload
      user.roles = user.organizations.last.roles
    end
  end
end
