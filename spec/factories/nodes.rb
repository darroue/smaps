FactoryBot.define do
  factory :node do
    friendly_name
    hostname
    guid
    auth_id
    active
    uninstall
    debug_mode
    last_connected

    trait :invalid do
      invalid_friendly_name
      # invalid_hostname
      # invalid_guid
      # invalid_auth_id
      invalid_active
      # invalid_uninstall
    end
  end
end
