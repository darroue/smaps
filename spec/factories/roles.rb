FactoryBot.define do
  factory :role do
    name
    permissions

    trait :invalid do
      invalid_name
      invalid_permissions
    end
  end
end
