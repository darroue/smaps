def build_traits(traits, prefix = nil, &block)
  traits.each do |trait_name|
    trait prefix ? :"#{prefix}_#{trait_name}" : trait_name do
      self.send(trait_name) do
        yield
      end
    end
  end
end

FactoryBot.define do
  # Valid

  build_traits %i(name package_name) do
    Faker::App.name
  end

  trait :version do
    version { Faker::App.semantic_version }
  end

  trait :friendly_name do
    friendly_name { Faker::FunnyName.name }
  end

  trait :hostname do
    hostname { Faker::Internet.domain_name }
  end

  build_traits %i(guid auth_id encrypted_password reset_password_token uninstall_string) do
    Faker::Crypto.sha1
  end

  trait :valid_until do
    valid_until { 1.week.from_now }
  end

  build_traits %i(active forced uninstall debug_mode) do
    Faker::Boolean.boolean
  end

  trait :usages do
    usages { 1 }
  end

  trait :node_id do
    node_id {
      create(:node, **{organization: create(:organization)}).id
    }
  end

  trait :user_id do
    user_id {
      create(:user, **{password: Faker::Internet.password}).id
    }
  end

  trait :email do
    email { Faker::Internet.email }
  end

  trait :password do
    password { Faker::Internet.password }
  end

  trait :password_confirmation do
    password_confirmation { password }
  end

  trait :permissions do
    permissions { ::RolesHelper::NEW_USERS_PERMISSIONS }
  end

  trait :invite_type do
    invite_type { %i(node user).sample }
  end

  trait :app_type do
    app_type { %i(chocolatey other).sample }
  end
  
  build_traits %i(last_connected remember_created_at reset_password_sent_at) do
    -10.minutes.from_now
  end

  trait :first_name do
    first_name { Faker::Name.first_name}
  end

  trait :last_name do
    last_name { Faker::Name.last_name}
  end

  trait :full_name do
    full_name { Faker::Name.name}
  end

  # Invalid
  build_traits %i(name first_name last_name package_name version friendly_name), :invalid do
    Faker::Lorem.characters(number: 51)
  end

  trait :invalid_app_name do
    name { Faker::Lorem.characters(number: 101) }
  end

  trait :invalid_active do
    active { :other }
  end

  trait :invalid_forced do
    forced { :other }
  end

  trait :invalid_usages do
    usages { 101 }
  end

  trait :invalid_auth_id do
    auth_id { :invalid }
  end

  trait :invalid_valid_until do
    valid_until { -1.day.from_now }
  end

  trait :invalid_email do
    email { :invalid }
  end

  trait :invalid_permissions do
    permissions { [] }
  end
end
