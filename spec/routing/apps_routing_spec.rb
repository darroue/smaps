require 'rails_helper'

RSpec.describe AppsController, type: :routing do
  let (:organization) do
    create(:organization)
  end

  let (:organization_id) do
    organization.id.to_s
  end

  describe 'routing' do
    it 'routes to #index' do
      expect(get: "/organizations/#{organization_id}/apps").to route_to('apps#index', organization_id: organization_id)
    end

    it 'routes to #new' do
      expect(get: "/organizations/#{organization_id}/apps/new").to route_to('apps#new', organization_id: organization_id)
    end

    it 'routes to #show' do
      expect(get: "/organizations/#{organization_id}/apps/1").to route_to('apps#show', id: '1', organization_id: organization_id)
    end

    it 'routes to #edit' do
      expect(get: "/organizations/#{organization_id}/apps/1/edit").to route_to('apps#edit', id: '1', organization_id: organization_id)
    end

    it 'routes to #create' do
      expect(post: "/organizations/#{organization_id}/apps").to route_to('apps#create', organization_id: organization_id)
    end

    it 'routes to #update via PUT' do
      expect(put: "/organizations/#{organization_id}/apps/1").to route_to('apps#update', id: '1', organization_id: organization_id)
    end

    it 'routes to #update via PATCH' do
      expect(patch: "/organizations/#{organization_id}/apps/1").to route_to('apps#update', id: '1', organization_id: organization_id)
    end

    it 'routes to #destroy' do
      expect(delete: "/organizations/#{organization_id}/apps/1").to route_to('apps#destroy', id: '1', organization_id: organization_id)
    end
  end
end
