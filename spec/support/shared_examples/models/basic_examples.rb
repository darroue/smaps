def get_attributes(type = nil)
  params = [model_name]
  params << type if type

  attributes = attributes_for(*params)
  unless model_name == :user
    attributes.merge!(organization_id: organization.id)
  end

  attributes
end

RSpec.shared_context 'a basic model attributes' do
  let(:model) {
    described_class
  }

  let(:model_name) {
    model.to_s.underscore.to_sym
  }

  let(:organization) {
    create :organization
  }

  let(:expected_attributes) {
    get_attributes.except(:password).keys
  }

  let(:valid_attributes) {
    get_attributes
  }

  let(:invalid_attributes) {
    get_attributes :invalid
  }

  let(:subject) {
    model.new(invalid_attributes)
  }
end

RSpec.shared_examples 'a basic model' do |required_params|
  it "tests that new model has only expected attributes" do
    expect(
      model.new.attributes.symbolize_keys.except(*%i(id created_at updated_at)).keys
    ).to match_array(expected_attributes)
  end

  it "is valid with valid attributes" do
    expect(model.new(valid_attributes)).to be_valid
  end

  it "is invalid with invalid attributes" do
    expect(model.new(invalid_attributes)).to be_invalid
  end

  required_params.each do |missing_param|
    it "is not valid without a '#{missing_param}'" do
      expect(model.new(valid_attributes.except(missing_param))).to be_invalid
    end
  end
end
