def controller_name
  @controller_name ||= described_class.controller_name
end

def model_name
  @model_name ||= controller_name.singularize.to_sym
end

def model_name_s
  @model_name_s ||= model_name.to_s.classify
end

def model
  @model ||= Object.const_get(model_name_s)
end

def create_subject
  model.create! valid_attributes
end

def should_test?(action, only, except)
  actions = %i(index new create show edit update destroy)
  (only ? actions & only : (except ? actions - except : actions)).include?(action)
end

def generate_instances_of(model, amount, unrelated = false)
  array = []

  amount.times do
    array << Object.const_get(model.to_s.classify).create!(
      unrelated ? attributes_for(model) : default_params.merge(attributes_for(model))
    )
  end

  array
end

RSpec.shared_context 'a organization CRUD Controller' do
  let(:organization) do
    @admin.organizations.last
  end

  let(:default_params) do
    { organization_id: organization.id }
  end

  let(:valid_session) {}

  let(:valid_attributes) do
    default_params.merge(attributes_for(model_name))
  end

  let(:invalid_attributes) do
    default_params.merge(attributes_for(model_name, :invalid))
  end

  let(:new_attributes) do
    attrs = attributes_for(model_name)
    keys = attrs.keys.first(2)
    default_params.merge(attrs.slice(keys))
  end
end

RSpec.shared_examples 'a CRUD Controller with many' do |assigned_model_name_p, amount = 3, unrelated = false|
  let (:assigned_model_name) { assigned_model_name_p.to_s.singularize }
  let (:assigned_model_name_s) { assigned_model_name.classify }

  describe assigned_model_name_p.to_s.humanize.to_s do
    let (:assigned_instances_ids) { generate_instances_of(assigned_model_name, amount, unrelated).map(&:id) }
    let (:create_params) do
      {
        params: default_params.merge(
          model_name => valid_attributes,
          assigned_model_name_p => assigned_instances_ids
        ), session: valid_session
      }
    end

    describe 'POST #create' do
      it "creates a new #{model_name_s}" do
        post :create, **create_params
        expect(model.last.send("#{assigned_model_name}_ids")).to eq(assigned_instances_ids)
      end
    end

    describe 'POST #update' do
      let (:update_params)  do
        create_params[:params] = create_params[:params].merge(id: create_subject.id)
        create_params
      end

      it "updates #{model_name_s}" do
        post :update, **update_params
        expect(model.last.send("#{assigned_model_name}_ids")).to eq(assigned_instances_ids)
      end
    end
  end
end

RSpec.shared_examples 'a CRUD Controller' do |only: nil, except: nil, custom_subject: nil|
  let(:defalt_opts) do
    { params: default_params, session: valid_session }
  end

  if should_test?(:index, only, except)
    describe 'GET #index' do
      it 'returns a success response' do
        model.create! valid_attributes
        get :index, **defalt_opts
        expect(response).to be_successful
      end
    end
  end

  if should_test?(:new, only, except)
    describe 'GET #new' do
      it 'returns a success response' do
        get :new, **defalt_opts
        expect(response).to be_successful
      end
    end
  end

  describe '' do
    unless custom_subject
      let!(:subject) do
        create_subject
      end
    end

    let(:get_opts) do
      defalt_opts.merge(params: default_params.merge(id: subject.to_param))
    end

    if should_test?(:show, only, except)
      describe 'GET #show' do
        it 'returns a success response' do
          get :show, **get_opts
          expect(response).to be_successful
        end
      end
    end

    if should_test?(:edit, only, except)
      describe 'GET #edit' do
        it 'returns a success response' do
          get :edit, **get_opts
          expect(response).to be_successful
        end
      end
    end

    if should_test?(:destroy, only, except)
      describe 'DELETE #destroy' do
        it "destroys the requested #{model_name_s}" do
          expect do
            delete :destroy, **get_opts
          end.to change(model, :count).by(-1)
        end

        it "redirects to the #{model_name_s} list" do
          delete :destroy, **get_opts
          expect(response).to redirect_to(controller: controller_name, action: :index)
        end
      end
    end

    if should_test?(:update, only, except)
      describe 'PUT #update' do
        context 'with valid params' do
          let(:update_opts_new) do
            get_opts.merge(params: get_opts[:params].merge(model_name => new_attributes))
          end

          it "updates the requested #{model_name_s}" do
            put :update, **update_opts_new
            expect(model.last).to have_attributes(**new_attributes)
          end

          it "redirects to the #{model_name_s}" do
            put :update, **update_opts_new
            if controller_name == 'organizations'
              expect(response).to be_successful
            else
              expect(response).to redirect_to(controller: controller_name, action: :show)
            end
          end
        end

        context 'with invalid params' do
          let(:update_opts_invalid) do
            get_opts.merge(params: get_opts[:params].merge(model_name => invalid_attributes))
          end

          it "returns a success response (i.e. to display the 'edit' template)" do
            put :update, **update_opts_invalid
            expect(response).to be_successful
          end
        end
      end
    end

    if should_test?(:create, only, except)
      describe 'POST #create' do
        before(:each) do
          subject.destroy
        end

        context 'with valid params' do
          let(:create_opts_valid) do
            get_opts.merge(params: get_opts[:params].merge(model_name => valid_attributes))
          end

          it "creates a new #{model_name_s}" do
            expect do
              post :create, **create_opts_valid
            end.to change(model, :count).by(1)
          end

          it "redirects to the created #{model_name_s}" do
            post :create, **create_opts_valid
            expect(response).to redirect_to(controller: controller_name, action: :show, id: model.last.id)
          end
        end

        context 'with invalid params' do
          let(:create_opts_invalid) do
            get_opts.merge(params: get_opts[:params].merge(model_name => invalid_attributes))
          end

          it "returns a success response (i.e. to display the 'new' template)" do
            post :create, **create_opts_invalid
            expect(response).to be_successful
          end
        end
      end
    end
  end
end
