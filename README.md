# Table of Contents
- [Table of Contents](#table-of-contents)
- [About](#about)
  - [Language](#language)
- [Setup](#setup)
- [Contributing](#contributing)
- [Author](#author)

# About
Main reason why i decided to write this application was because there is no free (open-source) app that would allow you to manage software on your Windows PCs the way i want.

That is:

* easily
* trough web app
* with ability to user different:
    * organizations
    * users
    * roles
    * groups
* runs in docker
* has lightweight client that instruct [Chocolatey](https://chocolatey.org)

*This applications does (or will) all of the above and more.*

## Language
It's written in Ruby (client) and its framework Rails (server) [(#RoR)](https://rubyonrails.org/)

# Setup
All you need to do is:

1. Install `Docker` and `Docker-Compose`
2. Download repository
3. Create needed files

    **These files are not saved in git, so keep them safe!**
    
    * [.env](.env) containing following:

          DATABASE_USERNAME=NAME_OF_THE_USER_FOR_DB             # db_user
          DATABASE_PASSWORD=USER_PASSWORD_FOR_DB                # db_pass
          HOST=DOMAIN_ADDRESS                                   # mypage.example.com
          PROJECT_NAME=SOME_NAME                                # mypage
          EMAIL=EMAIL_FOR_LETSENCRYPT                           # example@example.com
          SECRET_KEY_BASE=STRING_USED_FOR_ENCRYPTING_COOKIES    # od  -vN "16" -An -tx1 /dev/urandom | tr -d " \n" ; echo
          RAILS_MASTER_KEY=USSED_TO_ENCRYPT_CREDENDIALS         # od  -vN "64" -An -tx1 /dev/urandom | tr -d " \n" ; echo


4. Initialize DB

        docker-compose run web bundle exec rails db:create db:migrate db:seed

5. Create needed docker network

        docker network create web

7. Start all services
    * Start Proxy

          docker-compose up -f docker-compose.proxy.yml -d                   # will start proxy that is serving content with self-signed SSL cert
          
          # OR
          
          docker-compose up -f docker-compose.proxy_with_letsencrypt.yml -d  # will start proxy that is serving content with SSL cert signed by LetsEncrypt
    * Start AppServer

          docker-compose up -d

That is it! Your instance in now accessible on domain address specified in `.env` file

# Contributing
I'll be very happy if any of you decide that you want to help move this project forward.

* Feel free to create [Pull Requests](https://bitbucket.org/darroue/smaps/pull-requests) if you think that you can do things better way.
* Or create [Issue](https://bitbucket.org/darroue/smaps/issues) if you want to add some functionality that you are missing.
* Or even just test app and give me [FeedBack](mailto:darroue@gmail.com&subject=FeedBack)

# Author
[Petr Radouš](mailto:darroue@gmail.com&subject=Contact) (Darroue)