class CreateInstalledApps < ActiveRecord::Migration[5.2]
  def change
    create_table :installed_apps do |t|
      t.belongs_to :organization, foreign_key: true
      t.belongs_to :node, foreign_key: true
      t.string :name
      t.string :version
      t.string :app_type

      t.timestamps
    end
  end
end
