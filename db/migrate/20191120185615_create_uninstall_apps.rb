class CreateUninstallApps < ActiveRecord::Migration[5.2]
  def change
    create_table :uninstall_apps do |t|
      t.references :organization, foreign_key: true
      t.string :name
      t.string :version
      t.string :app_type
      t.references :node, foreign_key: true

      t.timestamps
    end
  end
end
