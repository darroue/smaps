class CreateSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :settings do |t|
      t.string :name
      t.string :value
      t.string :object_type
      t.integer :object_id
    end
  end
end
