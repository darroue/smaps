class AddUsagesToInvite < ActiveRecord::Migration[5.2]
  def change
    add_column :invites, :usages, :integer, default: 1
  end
end
