class CreateNodeGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :node_groups do |t|
      t.string :name
      t.boolean :active
      t.belongs_to :organization, foreign_key: true

      t.timestamps
    end
  end
end
