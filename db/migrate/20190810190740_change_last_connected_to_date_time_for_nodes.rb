class ChangeLastConnectedToDateTimeForNodes < ActiveRecord::Migration[5.2]
  def change
    remove_column :nodes, :last_connected
    add_column :nodes, :last_connected, :datetime
  end
end
