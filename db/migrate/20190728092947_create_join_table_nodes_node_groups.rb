class CreateJoinTableNodesNodeGroups < ActiveRecord::Migration[5.2]
  def change
    create_join_table :nodes, :node_groups do |t|
      # t.index [:node_id, :node_group_id]
      # t.index [:node_group_id, :node_id]
    end
  end
end
