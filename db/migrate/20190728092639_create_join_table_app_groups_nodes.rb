class CreateJoinTableAppGroupsNodes < ActiveRecord::Migration[5.2]
  def change
    create_join_table :app_groups, :nodes do |t|
      # t.index [:app_group_id, :node_id]
      # t.index [:node_id, :app_group_id]
    end
  end
end
