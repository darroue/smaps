class AddDebugModeToNode < ActiveRecord::Migration[6.0]
  def change
    add_column :nodes, :debug_mode, :boolean, default: false
  end
end
