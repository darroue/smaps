class CreateJoinTableAppGroupsNodeGroups < ActiveRecord::Migration[5.2]
  def change
    create_join_table :app_groups, :node_groups do |t|
      # t.index [:app_group_id, :node_group_id]
      # t.index [:node_group_id, :app_group_id]
    end
  end
end
