class SetActiveStateAsDefault < ActiveRecord::Migration[5.2]
  def change
    %i(apps app_groups nodes node_groups).each do |table|
      change_column_default table, :active, from: false, to: true
    end
  end
end
