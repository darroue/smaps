class AddPermissionsToRoles < ActiveRecord::Migration[5.2]
  def change
    add_column :roles, :permissions, :string
  end
end
