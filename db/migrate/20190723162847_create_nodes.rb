class CreateNodes < ActiveRecord::Migration[5.2]
  def change
    create_table :nodes do |t|
      t.string :hostname
      t.string :friendly_name
      t.string :guid
      t.string :auth_id
      t.time :last_connected
      t.boolean :uninstall
      t.boolean :active
      t.belongs_to :organization, foreign_key: true

      t.timestamps
    end
    add_index :nodes, :guid, unique: true
    add_index :nodes, :auth_id, unique: true
  end
end
