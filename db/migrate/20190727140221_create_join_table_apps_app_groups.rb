class CreateJoinTableAppsAppGroups < ActiveRecord::Migration[5.2]
  def change
    create_join_table :apps, :app_groups do |t|
      # t.index [:app_id, :app_group_id]
      # t.index [:app_group_id, :app_id]
    end
  end
end
