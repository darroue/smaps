class CreateApps < ActiveRecord::Migration[5.2]
  def change
    create_table :apps do |t|
      t.string :name
      t.string :package_name
      t.string :version
      t.boolean :forced
      t.boolean :active
      t.belongs_to :organization, foreign_key: true

      t.timestamps
    end
  end
end
