class CreateInvites < ActiveRecord::Migration[5.2]
  def change
    create_table :invites do |t|
      t.string :type
      t.string :auth_id
      t.datetime :valid_until
      t.belongs_to :organization, foreign_key: true

      t.timestamps
    end
  end
end
