class CreateLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :logs do |t|
      t.text :content
      t.string :loggable_type
      t.integer :loggable_id

      t.timestamps
    end
  end
end
