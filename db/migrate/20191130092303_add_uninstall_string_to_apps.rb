class AddUninstallStringToApps < ActiveRecord::Migration[5.2]
  def change
    add_column :installed_apps, :uninstall_string, :string
    add_column :uninstall_apps, :uninstall_string, :string
  end
end
