class RenameTypeColumnInInvites < ActiveRecord::Migration[5.2]
  def self.up
    rename_column :invites, :type, :invite_type
  end
end
