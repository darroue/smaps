class CreateJoinTableNodeApp < ActiveRecord::Migration[5.2]
  def change
    create_join_table :nodes, :apps do |t|
      # t.index [:node_id, :app_id]
      # t.index [:app_id, :node_id]
    end
  end
end
