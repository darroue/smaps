class AddOrganizationToLogs < ActiveRecord::Migration[6.0]
  def change
    add_reference :logs, :organization, null: false, foreign_key: true
  end
end
