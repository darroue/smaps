# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_07_222419) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "app_groups", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: true
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_app_groups_on_organization_id"
  end

  create_table "app_groups_apps", id: false, force: :cascade do |t|
    t.bigint "app_id", null: false
    t.bigint "app_group_id", null: false
  end

  create_table "app_groups_node_groups", id: false, force: :cascade do |t|
    t.bigint "app_group_id", null: false
    t.bigint "node_group_id", null: false
  end

  create_table "app_groups_nodes", id: false, force: :cascade do |t|
    t.bigint "app_group_id", null: false
    t.bigint "node_id", null: false
  end

  create_table "apps", force: :cascade do |t|
    t.string "name"
    t.string "package_name"
    t.string "version"
    t.boolean "forced"
    t.boolean "active", default: true
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_apps_on_organization_id"
  end

  create_table "apps_nodes", id: false, force: :cascade do |t|
    t.bigint "node_id", null: false
    t.bigint "app_id", null: false
  end

  create_table "installed_apps", force: :cascade do |t|
    t.bigint "organization_id"
    t.bigint "node_id"
    t.string "name"
    t.string "version"
    t.string "app_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uninstall_string"
    t.index ["node_id"], name: "index_installed_apps_on_node_id"
    t.index ["organization_id"], name: "index_installed_apps_on_organization_id"
  end

  create_table "invites", force: :cascade do |t|
    t.string "invite_type"
    t.string "auth_id"
    t.datetime "valid_until"
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "node_id"
    t.integer "usages", default: 1
    t.index ["node_id"], name: "index_invites_on_node_id"
    t.index ["organization_id"], name: "index_invites_on_organization_id"
    t.index ["user_id"], name: "index_invites_on_user_id"
  end

  create_table "logs", force: :cascade do |t|
    t.text "content"
    t.string "loggable_type"
    t.integer "loggable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "organization_id", null: false
    t.index ["organization_id"], name: "index_logs_on_organization_id"
  end

  create_table "node_groups", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: true
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_node_groups_on_organization_id"
  end

  create_table "node_groups_nodes", id: false, force: :cascade do |t|
    t.bigint "node_id", null: false
    t.bigint "node_group_id", null: false
  end

  create_table "nodes", force: :cascade do |t|
    t.string "hostname"
    t.string "friendly_name"
    t.string "guid"
    t.string "auth_id"
    t.boolean "uninstall"
    t.boolean "active", default: true
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_connected"
    t.boolean "debug_mode", default: false
    t.index ["auth_id"], name: "index_nodes_on_auth_id", unique: true
    t.index ["guid"], name: "index_nodes_on_guid", unique: true
    t.index ["organization_id"], name: "index_nodes_on_organization_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "organizations_users", id: false, force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "user_id", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "permissions"
    t.index ["organization_id"], name: "index_roles_on_organization_id"
  end

  create_table "roles_users", id: false, force: :cascade do |t|
    t.bigint "role_id", null: false
    t.bigint "user_id", null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.string "object_type"
    t.integer "object_id"
  end

  create_table "uninstall_apps", force: :cascade do |t|
    t.bigint "organization_id"
    t.string "name"
    t.string "version"
    t.string "app_type"
    t.bigint "node_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uninstall_string"
    t.index ["node_id"], name: "index_uninstall_apps_on_node_id"
    t.index ["organization_id"], name: "index_uninstall_apps_on_organization_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "full_name"
    t.boolean "active", default: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "app_groups", "organizations"
  add_foreign_key "apps", "organizations"
  add_foreign_key "installed_apps", "nodes"
  add_foreign_key "installed_apps", "organizations"
  add_foreign_key "invites", "nodes"
  add_foreign_key "invites", "organizations"
  add_foreign_key "invites", "users"
  add_foreign_key "logs", "organizations"
  add_foreign_key "node_groups", "organizations"
  add_foreign_key "nodes", "organizations"
  add_foreign_key "roles", "organizations"
  add_foreign_key "uninstall_apps", "nodes"
  add_foreign_key "uninstall_apps", "organizations"
end
