### --- Build Phase --- ###
FROM ruby:2.7.0-alpine AS BUILD

ARG ROOT_PATH=/root
ARG DEV_PACKAGES="build-base tzdata postgresql-dev nodejs yarn"

# These are used only for Rails to precompile assets
ENV SECRET_KEY_BASE=21fb7205d746fd4e093291cc8fc9c87d
ENV RAILS_MASTER_KEY=ae25e2db6bbb94865c5109ea0ccf6b88b28cd2fee3c34174d1a41c7ddf039247168f79a6c90d4d2169d37ae65406080ddc7e47a8dd52b41027ed06df00430ecf
ENV RAILS_ENV=production
ENV NODE_ENV=production

WORKDIR $ROOT_PATH

COPY Gemfile* package.json yarn.lock ./

RUN grep "BUNDLED WITH" Gemfile.lock -A 1 | grep -oE "((\d+\.?){1,4})" > bundler_version \
  && ls . \

  && gem install bundler -v $(cat bundler_version) \

  && apk update \
  && apk add --no-cache --update $DEV_PACKAGES \

  && mkdir -p /vendor/bundle \
  && bundle install --path=/vendor/bundle \

  && yarn install

COPY . .

RUN bin/rails assets:precompile

### xxx Build Phase xxx ###

### --- Deploy Phase --- ###
FROM ruby:2.7.0-alpine

ARG ROOT_PATH=/root
ARG PACKAGES="tzdata postgresql-client nodejs bash"
ENV RAILS_ENV=production

WORKDIR $ROOT_PATH
EXPOSE 3000

COPY --from=BUILD $ROOT_PATH $ROOT_PATH
COPY --from=BUILD /vendor/bundle /vendor/bundle

RUN gem install bundler -v $(cat bundler_version) \
  && apk update \
  && apk upgrade \
  && apk add --update --no-cache $PACKAGES

CMD \
  bundle config path /vendor/bundle \
  && bin/rails db:migrate \
  && bin/rails s -b 0.0.0.0
### xxx Deploy Phase xxx ###
