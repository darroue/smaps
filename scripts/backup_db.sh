#!/bin/bash
script_directory=$(dirname "$0")
target_directory=$1
target_environment="production"
file_with_variables=$script_directory/../.env

function echo_and_exit {
    if [ "$1" == "text" ]
    then
        echo "$2"
    elif [ "$1" == "file" ]
    then
        echo "File $2 doesn't exist!"
    elif [ "$1" == "folder" ]
    then
        echo "Folder \"$2\" doesn't exist!"
    elif [ "$1" == "variable" ]
    then
        echo "Variable $2 is not set in $file_with_variables!"
    fi

    exit
}

if [ -z ${1+x} ];
then
    echo_and_exit "text" "
    This script creates DUMP of your database to folder

    \$1     target folder for backup
    \$2     database env you want to dump (production by default) [optional]

    Usage:
        backup_db ~/backup
        backup_db ~/backup development"
fi
if ! [ -z ${2+x} ]; then target_environment=$2; fi
if ! test -f "$file_with_variables"; then echo_and_exit "file" $file_with_variables; fi
if ! test -d "$target_directory"; then echo_and_exit "folder" $target_directory; fi

source $file_with_variables
if [ -z ${project_name+x} ]; then echo_and_exit "variable" "project_name"; fi
if [ -z ${db_user+x} ]; then echo_and_exit "variable" "db_user"; fi

echo "Backing up database for project $project_name"
filename="${project_name}__$(date +%Y%m%d_%H%M%S).sql"

cd $script_directory/../
docker-compose exec -T database bash -c "pg_dump -U ${db_user} ${project_name}_${target_environment}" > $target_directory/$filename

