how_many = ARGV.first&.to_i
data = `git log --no-merges --format="%H;%s"`
data = data.split("\n")
amount_of_commits = data.count
amount_of_commits_with_tags = `git tag -l`.split("\n").count
amount_of_commits_without_tags = amount_of_commits - amount_of_commits_with_tags

unless how_many && how_many != 0
    puts "You must specify how many commits to tag!\n\n"
    puts "There is currently #{amount_of_commits} commits."
    if amount_of_commits_without_tags >= 1
        puts "#{amount_of_commits_without_tags} of these do not have tags!"
    else
        puts "All of them have tags!"
    end
    abort
end

data.map! do |line|
    line.split(";")
end.reverse!

bug, feature = 0, 0

data.map! do |obj|
    commit, subject = obj
    if subject.match(/fix/i)
        bug += 1
    else
        feature += 1
    end
    
    ["git tag -f", [0, feature, bug].join("."), commit].join("\s")
end

if how_many > amount_of_commits
    puts "There is only #{amount_of_commits} commits!"
    abort
end

data[-how_many..-1].each do |command|
    system command
end