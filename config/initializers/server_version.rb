# git_version_tag = `git describe --tags`.strip
# SERVER_VERSION = git_version_tag.present? ? git_version_tag : "unknown"

versions = JSON.parse(File.read(Rails.root.join("public", "downloads").join("version.json")))

CLIENT_VERSION = versions['client']
SERVER_VERSION = versions['server'] || "unknown"
