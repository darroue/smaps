# This will ensure that all protected roles have needed permissions
begin
    ActiveRecord::Base.establish_connection
    ActiveRecord::Base.connection

    MigrationHelper.restore_permissions_for_protected_roles if ActiveRecord::Base.connected?
rescue => e
    puts %q(
E    Can't restore permissions for protected roles!
E    Connection to database failed!
E    
E    This is OK for very first run or TEST environment.
    )
end
