module MigrationHelper
  def restore_permissions_for_protected_roles
    Role.where(name: Role::PROTECTED_ROLES).each do |role|
      role.update_column :permissions, Role.const_get("#{role.name}_permissions".upcase)
    end
  end

  module_function :restore_permissions_for_protected_roles
end
