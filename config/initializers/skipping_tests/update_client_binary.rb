# This initializer will download client binary file that is served to nodes
return if !(Rails.env.production? || Rails.env.development?)

dir_path = Rails.root.join("public", "downloads")
client_version = JSON.parse(File.read(dir_path.join("version.json"))).symbolize_keys[:client]

extensions = %w(exe zip).each do |extension|
  filename = "#{client_version}.#{extension}"

  begin
    unless File.exists?(dir_path.join(filename))
      require "open-uri"
      require "fileutils"
      
      Rails.logger.info "[Client Update] Downloading newer version of client binary."
      File.open(dir_path.join(filename), "wb") do |file|
        file.write URI.open("https://bitbucket.org/darroue/smapc/downloads/#{filename}").read
      end
  
      binary_files = Rails.root.join("public", "downloads").glob("*.#{extension}").sort_by do |file| File.ctime(file) end
  
      binary_files[0..-4].map do |file|
        Rails.logger.info "[Client Update] Removing old binary file - #{file}"
        FileUtils.rm(file)
      end if binary_files.count > 3
  
      Rails.logger.info "[Client Update] Done - #{client_version}"
    end
  rescue => e
    FileUtils.rm(dir_path.join(filename))
    Rails.logger.fatal "[Client Update] Failed with:"
    Rails.logger.fatal "[Client Update] " + e.inspect
  end
end

