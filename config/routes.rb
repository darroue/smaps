# == Route Map
#
#
# E    Can't restore permissions for protected roles!
# E    Connection to database failed!
# E
# E    This is OK for very first run or TEST environment.
#
#                                   Prefix Verb   URI Pattern                                                                              Controller#Action
#                                   api_v1 GET    /api/v1/nodes/:auth_id(.:format)                                                         api/v1/nodes#show
#                                          POST   /api/v1/nodes/:auth_id(.:format)                                                         api/v1/nodes#update
#                                          POST   /api/v1/nodes/:auth_id/log(.:format)                                                     api/v1/nodes#log
#                    api_v1_nodes_register PUT    /api/v1/nodes/register(.:format)                                                         api/v1/nodes#register
#                                  install GET    /install/:auth_id(.:format)                                                              install#show
#                         new_user_session GET    /users/sign_in(.:format)                                                                 devise/sessions#new
#                             user_session POST   /users/sign_in(.:format)                                                                 devise/sessions#create
#                     destroy_user_session DELETE /users/sign_out(.:format)                                                                devise/sessions#destroy
#                 cancel_user_registration GET    /users/cancel(.:format)                                                                  user/registrations#cancel
#                    new_user_registration GET    /users/sign_up(.:format)                                                                 user/registrations#new
#                   edit_user_registration GET    /users/edit(.:format)                                                                    user/registrations#edit
#                        user_registration PATCH  /users(.:format)                                                                         user/registrations#update
#                                          PUT    /users(.:format)                                                                         user/registrations#update
#                                          DELETE /users(.:format)                                                                         user/registrations#destroy
#                                          POST   /users(.:format)                                                                         user/registrations#create
#                                    users GET    /users(.:format)                                                                         organizations#index
#         organization_node_uninstall_apps POST   /organizations/:organization_id/nodes/:node_id/uninstall_apps(.:format)                  uninstall_apps#create
#          organization_node_uninstall_app DELETE /organizations/:organization_id/nodes/:node_id/uninstall_apps/:id(.:format)              uninstall_apps#destroy
#        delete_all_organization_node_logs DELETE /organizations/:organization_id/nodes/:node_id/logs/delete_all(.:format)                 logs#delete_all
# delete_older_logs_organization_node_logs DELETE /organizations/:organization_id/nodes/:node_id/logs/delete_older_logs(.:format)          logs#delete_older_logs
#              keep_organization_node_logs DELETE /organizations/:organization_id/nodes/:node_id/logs/keep(.:format)                       logs#keep
#                       organization_nodes GET    /organizations/:organization_id/nodes(.:format)                                          nodes#index
#                                          POST   /organizations/:organization_id/nodes(.:format)                                          nodes#create
#                    new_organization_node GET    /organizations/:organization_id/nodes/new(.:format)                                      nodes#new
#                   edit_organization_node GET    /organizations/:organization_id/nodes/:id/edit(.:format)                                 nodes#edit
#                        organization_node GET    /organizations/:organization_id/nodes/:id(.:format)                                      nodes#show
#                                          PATCH  /organizations/:organization_id/nodes/:id(.:format)                                      nodes#update
#                                          PUT    /organizations/:organization_id/nodes/:id(.:format)                                      nodes#update
#                                          DELETE /organizations/:organization_id/nodes/:id(.:format)                                      nodes#destroy
#                        organization_logs GET    /organizations/:organization_id/logs(.:format)                                           logs#index
#                         organization_log GET    /organizations/:organization_id/logs/:id(.:format)                                       logs#show
#                                          DELETE /organizations/:organization_id/logs/:id(.:format)                                       logs#destroy
#             delete_all_organization_logs DELETE /organizations/:organization_id/logs/delete_all(.:format)                                logs#delete_all
#      delete_older_logs_organization_logs DELETE /organizations/:organization_id/logs/delete_older_logs/:age(.:format)                    logs#delete_older_logs
#                   keep_organization_logs DELETE /organizations/:organization_id/logs/keep/:amount(.:format)                              logs#keep
#                 organization_node_groups GET    /organizations/:organization_id/node_groups(.:format)                                    node_groups#index
#                                          POST   /organizations/:organization_id/node_groups(.:format)                                    node_groups#create
#              new_organization_node_group GET    /organizations/:organization_id/node_groups/new(.:format)                                node_groups#new
#             edit_organization_node_group GET    /organizations/:organization_id/node_groups/:id/edit(.:format)                           node_groups#edit
#                  organization_node_group GET    /organizations/:organization_id/node_groups/:id(.:format)                                node_groups#show
#                                          PATCH  /organizations/:organization_id/node_groups/:id(.:format)                                node_groups#update
#                                          PUT    /organizations/:organization_id/node_groups/:id(.:format)                                node_groups#update
#                                          DELETE /organizations/:organization_id/node_groups/:id(.:format)                                node_groups#destroy
#                        organization_apps GET    /organizations/:organization_id/apps(.:format)                                           apps#index
#                                          POST   /organizations/:organization_id/apps(.:format)                                           apps#create
#                     new_organization_app GET    /organizations/:organization_id/apps/new(.:format)                                       apps#new
#                    edit_organization_app GET    /organizations/:organization_id/apps/:id/edit(.:format)                                  apps#edit
#                         organization_app GET    /organizations/:organization_id/apps/:id(.:format)                                       apps#show
#                                          PATCH  /organizations/:organization_id/apps/:id(.:format)                                       apps#update
#                                          PUT    /organizations/:organization_id/apps/:id(.:format)                                       apps#update
#                                          DELETE /organizations/:organization_id/apps/:id(.:format)                                       apps#destroy
#                  organization_app_groups GET    /organizations/:organization_id/app_groups(.:format)                                     app_groups#index
#                                          POST   /organizations/:organization_id/app_groups(.:format)                                     app_groups#create
#               new_organization_app_group GET    /organizations/:organization_id/app_groups/new(.:format)                                 app_groups#new
#              edit_organization_app_group GET    /organizations/:organization_id/app_groups/:id/edit(.:format)                            app_groups#edit
#                   organization_app_group GET    /organizations/:organization_id/app_groups/:id(.:format)                                 app_groups#show
#                                          PATCH  /organizations/:organization_id/app_groups/:id(.:format)                                 app_groups#update
#                                          PUT    /organizations/:organization_id/app_groups/:id(.:format)                                 app_groups#update
#                                          DELETE /organizations/:organization_id/app_groups/:id(.:format)                                 app_groups#destroy
#             edit_users_organization_role GET    /organizations/:organization_id/roles/:id/edit_users(.:format)                           roles#edit_users
#                                          POST   /organizations/:organization_id/roles/:id/edit_users(.:format)                           roles#save_users
#                       organization_roles GET    /organizations/:organization_id/roles(.:format)                                          roles#index
#                                          POST   /organizations/:organization_id/roles(.:format)                                          roles#create
#                    new_organization_role GET    /organizations/:organization_id/roles/new(.:format)                                      roles#new
#                   edit_organization_role GET    /organizations/:organization_id/roles/:id/edit(.:format)                                 roles#edit
#                        organization_role GET    /organizations/:organization_id/roles/:id(.:format)                                      roles#show
#                                          PATCH  /organizations/:organization_id/roles/:id(.:format)                                      roles#update
#                                          PUT    /organizations/:organization_id/roles/:id(.:format)                                      roles#update
#                                          DELETE /organizations/:organization_id/roles/:id(.:format)                                      roles#destroy
#               accept_organization_invite GET    /organizations/:organization_id/invites/:id/accept(.:format)                             invites#accept
#               reject_organization_invite GET    /organizations/:organization_id/invites/:id/reject(.:format)                             invites#reject
#                renew_organization_invite GET    /organizations/:organization_id/invites/:id/renew(.:format)                              invites#renew
#                     organization_invites GET    /organizations/:organization_id/invites(.:format)                                        invites#index
#                                          POST   /organizations/:organization_id/invites(.:format)                                        invites#create
#                  new_organization_invite GET    /organizations/:organization_id/invites/new(.:format)                                    invites#new
#                 edit_organization_invite GET    /organizations/:organization_id/invites/:id/edit(.:format)                               invites#edit
#                      organization_invite GET    /organizations/:organization_id/invites/:id(.:format)                                    invites#show
#                                          PATCH  /organizations/:organization_id/invites/:id(.:format)                                    invites#update
#                                          PUT    /organizations/:organization_id/invites/:id(.:format)                                    invites#update
#                                          DELETE /organizations/:organization_id/invites/:id(.:format)                                    invites#destroy
#                            organizations GET    /organizations(.:format)                                                                 organizations#index
#                                          POST   /organizations(.:format)                                                                 organizations#create
#                         new_organization GET    /organizations/new(.:format)                                                             organizations#new
#                        edit_organization GET    /organizations/:id/edit(.:format)                                                        organizations#edit
#                             organization GET    /organizations/:id(.:format)                                                             organizations#show
#                                          PATCH  /organizations/:id(.:format)                                                             organizations#update
#                                          PUT    /organizations/:id(.:format)                                                             organizations#update
#                                          DELETE /organizations/:id(.:format)                                                             organizations#destroy
#                                     root GET    /                                                                                        organizations#index
#            rails_postmark_inbound_emails POST   /rails/action_mailbox/postmark/inbound_emails(.:format)                                  action_mailbox/ingresses/postmark/inbound_emails#create
#               rails_relay_inbound_emails POST   /rails/action_mailbox/relay/inbound_emails(.:format)                                     action_mailbox/ingresses/relay/inbound_emails#create
#            rails_sendgrid_inbound_emails POST   /rails/action_mailbox/sendgrid/inbound_emails(.:format)                                  action_mailbox/ingresses/sendgrid/inbound_emails#create
#      rails_mandrill_inbound_health_check GET    /rails/action_mailbox/mandrill/inbound_emails(.:format)                                  action_mailbox/ingresses/mandrill/inbound_emails#health_check
#            rails_mandrill_inbound_emails POST   /rails/action_mailbox/mandrill/inbound_emails(.:format)                                  action_mailbox/ingresses/mandrill/inbound_emails#create
#             rails_mailgun_inbound_emails POST   /rails/action_mailbox/mailgun/inbound_emails/mime(.:format)                              action_mailbox/ingresses/mailgun/inbound_emails#create
#           rails_conductor_inbound_emails GET    /rails/conductor/action_mailbox/inbound_emails(.:format)                                 rails/conductor/action_mailbox/inbound_emails#index
#                                          POST   /rails/conductor/action_mailbox/inbound_emails(.:format)                                 rails/conductor/action_mailbox/inbound_emails#create
#        new_rails_conductor_inbound_email GET    /rails/conductor/action_mailbox/inbound_emails/new(.:format)                             rails/conductor/action_mailbox/inbound_emails#new
#       edit_rails_conductor_inbound_email GET    /rails/conductor/action_mailbox/inbound_emails/:id/edit(.:format)                        rails/conductor/action_mailbox/inbound_emails#edit
#            rails_conductor_inbound_email GET    /rails/conductor/action_mailbox/inbound_emails/:id(.:format)                             rails/conductor/action_mailbox/inbound_emails#show
#                                          PATCH  /rails/conductor/action_mailbox/inbound_emails/:id(.:format)                             rails/conductor/action_mailbox/inbound_emails#update
#                                          PUT    /rails/conductor/action_mailbox/inbound_emails/:id(.:format)                             rails/conductor/action_mailbox/inbound_emails#update
#                                          DELETE /rails/conductor/action_mailbox/inbound_emails/:id(.:format)                             rails/conductor/action_mailbox/inbound_emails#destroy
#    rails_conductor_inbound_email_reroute POST   /rails/conductor/action_mailbox/:inbound_email_id/reroute(.:format)                      rails/conductor/action_mailbox/reroutes#create
#                       rails_service_blob GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
#                rails_blob_representation GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#                       rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
#                update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#                     rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get "/nodes/:auth_id", to: "nodes#show"
      post "/nodes/:auth_id", to: "nodes#update"
      post "/nodes/:auth_id/log", to: "nodes#log"
      put "/nodes/register", to: "nodes#register"
    end
  end

  resources :install, only: [:show], param: :auth_id
  
  devise_for :users, controllers: {
    registrations: 'user/registrations'
  }

  get 'users', to: "organizations#index"

  resources :organizations do
    resources :nodes do
      resources :uninstall_apps, only: %i(create destroy)
      resource :logs, only: [] do
        delete :delete_all
        delete :delete_older_logs
        delete :keep
      end
    end

    resources :logs, only: %i(show index destroy)
    resource :logs, only: [] do
      delete :delete_all
      delete "delete_older_logs/:age", action: :delete_older_logs, as: :delete_older_logs
      delete "keep/:amount", action: :keep, as: :keep
    end

    resources :node_groups
    resources :apps
    resources :app_groups
    resources :roles do
      member do
        get "/edit_users", to: "roles#edit_users"
        post "/edit_users", to: "roles#save_users"
      end
    end
    resources :invites do
      member do
        get "/accept", action: :accept
        get "/reject", action: :reject
        get "/renew", action: :renew
      end
    end
  end
  root controller: :organizations, action: :index
end
