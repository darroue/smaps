// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("@rails/activestorage").start()
require("channels")

// jQuery
require('jquery')
import "jquery-ui"
require('jquery-ui')

// Font-Awesome
require('@fortawesome/fontawesome-free/js/all');

// Bootstrap
require("bootstrap")
require("bootstrap-table")
import 'bootstrap/scss/bootstrap'

// Bootstrap-table
import "bootstrap-table/dist/bootstrap-table"

// Theme
require("startbootstrap-sb-admin-2/js/sb-admin-2.min.js")
import("startbootstrap-sb-admin-2/css/sb-admin-2.min.css")

require.context('startbootstrap-sb-admin-2/img/', true)
require.context('../images', true)

$(
  function () {
    $(".collapse-item.active").closest('.collapse').addClass('show')
    $("a[data-toggle='collapse']").on("click", function () {
      $($(this).data('target')).collapse('toggle')
    })
    $("[data-toggle='dropdown']").not('.loaded').on("click", function () {
      const $element = $(this)
      if (!$element.closest('.pagination-detail').length) {

        $element.dropdown('toggle')
        $element.addClass('loaded')
      }
    })
  }
)
