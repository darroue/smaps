# == Schema Information
# Schema version: 20210804192444
#
# Table name: logs
#
# @!attribute id
#   @return []
# @!attribute content
#   @return [String]
# @!attribute loggable_type
#   @return [String]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
# @!attribute loggable_id
#   @return [Integer]
# @!attribute organization_id
#   @return []
#
# Indexes
#
#  index_logs_on_organization_id  (organization_id)
#
class Log < ApplicationRecord
  belongs_to :loggable, polymorphic: true
  belongs_to :organization

  validates :content, presence: true

  scope :for_today, -> () {
    where(created_at: Time.now.all_day)
  }

  scope :older_than, -> (number_of_days) {
    where(created_at: Time.at(0)..(Time.now - number_of_days.days).end_of_day)
  }

  scope :keep, ->(amount) {
    ids = order(created_at: :desc).group_by do |log|
            log.loggable_id
          end.map do |id, logs|
            logs.first(amount)
          end.flatten.map(&:id)

    where.not(id: ids)
  }

  def severity
    %i(
      fatal
      error
      info
      debug
    ).each do |type|
      return type if content.include?(type.to_s.upcase)
    end
    
    nil
  end

  def entity
    loggable
  end

  def log_created_at
    I18n.l(created_at)
  end
end
