# == Schema Information
# Schema version: 20210804192444
#
# Table name: nodes
#
# @!attribute id
#   @return []
# @!attribute active
#   @return [Boolean]
# @!attribute debug_mode
#   @return [Boolean]
# @!attribute friendly_name
#   @return [String]
# @!attribute guid
#   @return [String]
# @!attribute hostname
#   @return [String]
# @!attribute last_connected
#   @return [Time]
# @!attribute uninstall
#   @return [Boolean]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
# @!attribute auth_id
#   @return [String]
# @!attribute organization_id
#   @return []
#
# Indexes
#
#  index_nodes_on_auth_id          (auth_id) UNIQUE
#  index_nodes_on_guid             (guid) UNIQUE
#  index_nodes_on_organization_id  (organization_id)
#
class Node < ApplicationRecord
  PERMITED_FIELDS = %i(friendly_name uninstall active debug_mode server_url schedule)

  belongs_to :organization
  has_one :invite, dependent: :destroy
  has_many :installed_apps, dependent: :destroy
  has_many :uninstall_apps, dependent: :destroy
  has_many :settings, as: :object, dependent: :destroy
  has_and_belongs_to_many :app_groups
  has_and_belongs_to_many :node_groups
  has_and_belongs_to_many :apps

  validates :friendly_name, presence: true, length: { minimum: 2, maximum: 50 }

  after_create :create_invite
  
  alias_attribute :name, :friendly_name
  alias_attribute :display_name, :friendly_name

  COLLECTIONS = %w(app_groups node_groups apps)
  COLLECTION_ATTRIBUTES = %w(friendly_name id)
  
  include Collectable
  include Loggable
  include Settings::Client

  def create_invite
    organization.invites.create!(
      invite_type: :node,
      usages: 1,
      node_id: id
    )
  end

  def last_connected_at
    return unless last_connected

    I18n.l(last_connected)
  end
end
