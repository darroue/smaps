# == Schema Information
# Schema version: 20210804192444
#
# Table name: invites
#
# @!attribute id
#   @return []
# @!attribute invite_type
#   @return [String]
# @!attribute usages
#   @return [Integer]
# @!attribute valid_until
#   @return [Time]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
# @!attribute auth_id
#   @return [String]
# @!attribute node_id
#   @return []
# @!attribute organization_id
#   @return []
# @!attribute user_id
#   @return []
#
# Indexes
#
#  index_invites_on_node_id          (node_id)
#  index_invites_on_organization_id  (organization_id)
#  index_invites_on_user_id          (user_id)
#
class Invite < ApplicationRecord
  include Defaultable

  PERMITED_FIELDS = %i(invite_type valid_until usages email)
  DEFAULT_VALUES = {
    auth_id: Digest::SHA256.hexdigest(Time.now.to_s),
    usages: 1
  }

  belongs_to :organization
  belongs_to :user, required: false
  belongs_to :node, required: false

  validates :organization, :invite_type, :auth_id, :usages, presence: true
  validates :valid_until, presence: true, if: -> () { node_id.nil? }
  validates :usages, numericality: { less_than_or_equal_to: 100, only_integer: true }
  validate :email_validation
  validates_uniqueness_of :user_id, scope: :organization, message: "already invited!", if: lambda { user_id.present? }

  after_initialize -> (){ set_default_values %i(auth_id invite_type)}

  scope :usable, lambda {
    where("usages >= 1 AND (valid_until = NULL OR valid_until >= :now)", now: DateTime.now)
  }

  scope :node, lambda {
    where(invite_type: :node)
  }

  scope :user, lambda {
    where(invite_type: :user)
  }

  def usable?
    usages >= 1 && (
      valid_until.nil? || valid_until >= DateTime.now
    )
  end

  def use
    self.usages = [0, usages - 1].max
    save
  end

  def identificator
    case invite_type
    when "user"
      user.email
    when "node"
      node ? node.friendly_name : "Node invite #{id}"
    end
  end

  def email=(value)
    @email = value
  end

  def email
    @email || user&.email
  end

  def invite_valid_until
    usable? ? I18n.l(valid_until) : "<s>#{valid_until}</s>"
  end

  def remaining_usages
    usages
  end

  def node_id_collection(organization)
    organization.nodes.collect do |node|
      [
        node.friendly_name,
        node.id
      ]
    end
  end

  def node_id_collection_selected
    [node_id]
  end

  def invite_type_collection(_)
    %w(user node).map do |type|
      [
        type.humanize,
        type
      ]
    end
  end

  def invite_type_collection_selected
    [invite_type]
  end

  def email_validation
    if invite_type == 'user'
      if email.present?
        user = User.find_by(email: email)

        if user
          if organization.user_ids.include?(user.id)
            errors.add(:user, 'is already member of organization!')
          else
            self[:user_id] = user.id
          end
        else
          errors.add(:user, 'not found!')
        end
      else
        errors.add(:email, :empty)
      end
    else
      self[:user_id] = nil
    end
  end

  def node_install_url
    if invite_type == 'node'
      Rails.application.routes.url_helpers.install_url(auth_id, host: ENV['HOST'])
    end
  end
end
