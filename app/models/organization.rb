# == Schema Information
# Schema version: 20210804192444
#
# Table name: organizations
#
# @!attribute id
#   @return []
# @!attribute name
#   @return [String]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
#
class Organization < ApplicationRecord
  has_and_belongs_to_many :users
  has_many :invites

  PERMITED_FIELDS = %i(name current_user_id users server_url schedule)

  %i(roles apps app_groups nodes node_groups invites logs).each do |child|
    has_many child, dependent: :destroy
  end

  has_many :settings, as: :object, dependent: :destroy

  attr_accessor :seeding, :current_user_id

  validate :protected_names, unless: :seeding
  validates :name, presence: true, allow_blank: false, length: { minimum: 2, maximum: 50 }

  before_save :clean_data
  after_create :set_user_and_roles, if: -> () { current_user_id }

  include ::OrganizationsHelper
  include ::RolesHelper
  include Settings::Client

  def protected_names
    errors.add :name, :protected, name: name if self.name.downcase.in?(PROTECTED_NAMES)
  end

  def set_user_and_roles
    current_user = User.find(current_user_id)

    self.users << current_user
    create_protected_roles.each do |role|
      role.users << current_user
    end
  end

  def create_protected_roles
    PROTECTED_ROLES.map do |role|
      roles.create(
        name: role,
        permissions: Role.const_get("#{role}_permissions".gsub("\s", '_').upcase)
      )
    end
  end

  def users_validation(change)
    if persisted? && change.to_a == []
      errors.add(:base, 'You cant remove last user from organization!')

      false
    else
      true
    end
  end

  def destroy
    if nodes.any?
      errors.add(:base, "Organization have existing nodes!<br/>If you want to really destroy organization, then you have to destroy all nodes first.")

      false
    else
      super
    end
  end

  def users_collection(organization)
    organization.users.map do |user|
      [
        user.full_name,
        user.id
      ]
    end
  end

  def users_collection_selected
    user_ids
  end
end
