# == Schema Information
# Schema version: 20210804192444
#
# Table name: uninstall_apps
#
# @!attribute id
#   @return []
# @!attribute app_type
#   @return [String]
# @!attribute name
#   @return [String]
# @!attribute uninstall_string
#   @return [String]
# @!attribute version
#   @return [String]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
# @!attribute node_id
#   @return []
# @!attribute organization_id
#   @return []
#
# Indexes
#
#  index_uninstall_apps_on_node_id          (node_id)
#  index_uninstall_apps_on_organization_id  (organization_id)
#
class UninstallApp < ApplicationRecord
  belongs_to :organization
  belongs_to :node

  validates :version, :app_type, presence: true, length: { minimum: 2, maximum: 50 }
  validates :name, presence: true, length: { minimum: 2, maximum: 100 }
  validates :node, presence: true
  validates_uniqueness_of :name, scope: [:organization, :version]
  validates_uniqueness_of :version, scope: [:organization, :name]

  after_initialize lambda { |ua| ua.organization = ua.node.organization }, if: lambda { self.new_record? }
end
