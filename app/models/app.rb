# == Schema Information
# Schema version: 20210804192444
#
# Table name: apps
#
# @!attribute id
#   @return []
# @!attribute active
#   @return [Boolean]
# @!attribute forced
#   @return [Boolean]
# @!attribute name
#   @return [String]
# @!attribute package_name
#   @return [String]
# @!attribute version
#   @return [String]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
# @!attribute organization_id
#   @return []
#
# Indexes
#
#  index_apps_on_organization_id  (organization_id)
#
class App < ApplicationRecord
  # Application is basic object that can be assigned to Node or Application Group
  # in order to generate list of applications you want to install on node.
  # @see Node
  # @see AppGroup
  #
  # @!attribute name
  #   @return [String] name of the application

  # @!attribute package_name
  #   @return [String] name of the package

  # @!attribute version
  # @note
  #   Can only contain numbers in groups between 1 to 8 characters, delimited by dot. Or be left completely empty.
  # @example
  #   "xxx.xxx.xxx.xxx"
  #   "x.x"
  #   "x"
  #   ""
  # @return [String] package version

  # @!attribute active
  #   Marks application for installation
  #   @return [Boolean]

  # @!attribute forced
  #   Marks application for forced installation
  #   @return [Boolean]

  # @!attribute organization
  #   @see Organization

  # @!attribute app_groups
  #   AppGroups that this app is member of
  #   @see AppGroup

  # @!attribute nodes
  #   Nodes that have this app assigned
  #   @see Node

  COLLECTIONS = %w(app_groups nodes)
  COLLECTION_ATTRIBUTES = %w(name id)
  
  include Collectable
  include Defaultable

  PERMITED_FIELDS = %i(name package_name version forced active)

  belongs_to :organization
  has_and_belongs_to_many :app_groups
  has_and_belongs_to_many :nodes

  after_initialize -> (){ set_default_values %i(version forced active)}
  
  validates :name, :package_name, presence: true, length: { minimum: 2, maximum: 50 }
  validates :version, allow_blank: true, format: { with: /\A(\d{1,8}\.)?(\d{1,8}\.)?(\d{1,8}\.)?(\d{1,8})\z/, message: "is in invalid format!" }
  validates_uniqueness_of :version, scope: [:organization, :package_name], message: "must be unique for Package name!"
  validates_inclusion_of :active, :forced, in: [true, false]

  # Returns app *name* and *version*
  # @return [String] "AppName (Version)"
  def name_with_version
    "#{name} (#{version!})"
  end

  # Returns app *version* or "latest"
  # @return [String] actual version
  # @return [String] "latest" if version is empty
  def version!
    version.present? ? version : "latest"
  end
end
