# == Schema Information
# Schema version: 20210804192444
#
# Table name: app_groups
#
# @!attribute id
#   @return []
# @!attribute active
#   @return [Boolean]
# @!attribute name
#   @return [String]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
# @!attribute organization_id
#   @return []
#
# Indexes
#
#  index_app_groups_on_organization_id  (organization_id)
#
class AppGroup < ApplicationRecord
  # Application Group ties together multiple Application instances for easier management
  # @see App
  #
  # @!attribute name
  #   @return [String] name of the application group

  # @!attribute active
  #   Marks application as active
  #   @return [Boolean]

  # @!attribute organization
  #   @see Organization

  # @!attribute apps
  #   Apps that are members of this group
  #   @see App

  # @!attribute nodes
  #   Nodes that are members of this group
  #   @see Node

  # @!attribute node_groups
  #   NodeGroups that are members of this group
  #   @see NodeGroup

  PERMITED_FIELDS = %i(name active)

  belongs_to :organization
  has_and_belongs_to_many :apps
  has_and_belongs_to_many :node_groups

  validates :name, presence: true, length: { minimum: 2, maximum: 50 }
  validates_inclusion_of :active, in: [true, false]

  COLLECTIONS = %w(apps node_groups)
  COLLECTION_ATTRIBUTES = %w(name id)
  
  include Collectable
end
