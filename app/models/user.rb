# == Schema Information
# Schema version: 20210804192444
#
# Table name: users
#
# @!attribute id
#   @return []
# @!attribute active
#   @return [Boolean]
# @!attribute email
#   @return [String]
# @!attribute encrypted_password
#   @return [String]
# @!attribute first_name
#   @return [String]
# @!attribute full_name
#   @return [String]
# @!attribute last_name
#   @return [String]
# @!attribute remember_created_at
#   @return [Time]
# @!attribute reset_password_sent_at
#   @return [Time]
# @!attribute reset_password_token
#   @return [String]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :recoverable, :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :validatable, :timeoutable, :rememberable

  has_and_belongs_to_many :organizations
  has_and_belongs_to_many :roles
  has_many :invites

  validates_presence_of :first_name, :last_name, :full_name
  validates :email, uniqueness: true, presence: true

  after_validation lambda { |u| u.organizations << Organization.first }, on: :create
  before_validation lambda { |u| u.full_name = "#{first_name} #{last_name}" }

  COLLECTIONS = %w()
  COLLECTION_ATTRIBUTES = %w(full_name id)
  
  include Collectable

  attr_reader :permissions

  def user_organizations
    organizations.where.not(name: "Default")
  end

  def invite(organization)
    invites.find_by organization: organization
  end

  def permissions(organization)
    roles.where(organization: organization).pluck(:permissions).flatten.uniq.sort
  end

  def self.collection(organization)
    organization.users.order(last_name: :asc).collect do |rel|
      collection_attributes = rel.class::COLLECTION_ATTRIBUTES

      [
        rel.send(collection_attributes.first.to_sym),
        rel.send(collection_attributes.last.to_sym)
      ]
    end
  end
end
