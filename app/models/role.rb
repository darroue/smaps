# == Schema Information
# Schema version: 20210804192444
#
# Table name: roles
#
# @!attribute id
#   @return []
# @!attribute name
#   @return [String]
# @!attribute permissions
#   @return [String]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
# @!attribute organization_id
#   @return []
#
# Indexes
#
#  index_roles_on_organization_id  (organization_id)
#
class Role < ApplicationRecord
  belongs_to :organization
  has_and_belongs_to_many :users

  PERMITED_FIELDS = { name: nil, permissions: [] }
  COLLECTIONS = %w(users)
  COLLECTION_ATTRIBUTES = %w(name id)
  
  include Collectable
  
  serialize :permissions, Array
  validates :name, presence: true, length: { minimum: 2, maximum: 50 }
  validates_uniqueness_of :name, scope: :organization
  validate :role_is_not_protected, :sanitize_permissions

  include ::RolesHelper

  def name_with_user_ids
    { name => user_ids }
  end

  def protected?
    name.in?(PROTECTED_ROLES)
  end

  def role_is_not_protected
    errors.add :base, "You can't #{validation_context || :destroy} protected role" if protected? && persisted?
  end

  def sanitize_permissions
    self.update_attribute :permissions, permissions & ALL_PERMISSIONS
  end

  def permissions_collection(_)
    Role::PERMISSIONS.values.flatten.map do |permission|
      [
        permission.to_s.humanize,
        permission
      ]
    end
  end

  def permissions_collection_selected
    permissions
  end

  def destroy
    role_is_not_protected
    if errors.present? && !@destroyed_by_association.present?
      return false
    else
      super
    end
  end
end
