require 'active_support/concern'

# This module allows setting default values for fields by their type or by specifing valued in DEFAULT_VALUES constant
# It needs fo be called like:
#   before_validation -> (){ set_default_attributes %i(field1 field2)}
module Defaultable
    extend ActiveSupport::Concern
    SKIP_DEFAULT_FOR = %i(id created_at updated_at)
    
    included do |base|
        base.const_set :DEFAULT_VALUES_BY_TYPE, base.columns.collect {|c| { c.name.to_sym => default_value_for_type(c.type) }}.inject(&:merge).except(*SKIP_DEFAULT_FOR)
    end
    
    def set_default_values(fields)
        clazz = self.class
        constant = defined?(clazz::DEFAULT_VALUES) ? clazz::DEFAULT_VALUES_BY_TYPE.merge(clazz::DEFAULT_VALUES) : clazz::DEFAULT_VALUES_BY_TYPE
        empty_fields = self.attributes.symbolize_keys.slice(*fields).select(& ->(k,v) { !v.present? }).keys

        assign_attributes(constant.slice(*empty_fields))
    end

    class_methods do
        def default_value_for_type(type_name)
          case type_name
          when :string
              ""
          when :boolean
              false
          when :integer
              0
          end
        end
    end
end
