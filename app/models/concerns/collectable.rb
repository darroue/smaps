require 'active_support/concern'

module Collectable
  extend ActiveSupport::Concern
  
  included do |base|
    def self.collection(organization)
      where(organization: organization).order(name: :asc).collect do |rel|
        collection_attributes = rel.class::COLLECTION_ATTRIBUTES

        [
          rel.send(collection_attributes.first.to_sym),
          rel.send(collection_attributes.last.to_sym)
        ]
      end
    end
  
    base::COLLECTIONS.each do |multiple|
      one = multiple.singularize
  
      clazz = one.classify.constantize
  
      define_method("#{multiple}_collection") do |organization|
        clazz.collection(organization)
      end
  
      define_method("#{multiple}_collection_selected") do
        send(:"#{one}_ids")
      end
    end
  end
end
