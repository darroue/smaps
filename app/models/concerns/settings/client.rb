require 'active_support/concern'

module Settings
  module Client
    extend ActiveSupport::Concern
    
    included do |base|
      validate :schedule_format

      def schedule_format
        if schedule.present?
          unless schedule.match(/^(\d{2}:\d{2}:\d{2},?)+$/)
            errors.add(:schedule, 'is in invalid format! Valid format is: 00:00:00,..')
          end
        end
      end

      def server_url
        settings.find_by(name: :server_url)&.value
      end
    
      def server_url=(value)
        setting = settings.find_or_initialize_by(name: :server_url)
    
        if value.present?
          setting.value = value
          setting.save
        else
          setting.destroy
        end
      end
    
      def schedule
        settings.find_by(name: :schedule)&.value
      end
    
      def schedule=(value)
        setting = settings.find_or_initialize_by(name: :schedule)
    
        if value.present?
          setting.value = value
          setting.save
        else
          setting.destroy
        end
      end
    end
  end
end
