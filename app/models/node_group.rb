# == Schema Information
# Schema version: 20210804192444
#
# Table name: node_groups
#
# @!attribute id
#   @return []
# @!attribute active
#   @return [Boolean]
# @!attribute name
#   @return [String]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
# @!attribute organization_id
#   @return []
#
# Indexes
#
#  index_node_groups_on_organization_id  (organization_id)
#
class NodeGroup < ApplicationRecord
  PERMITED_FIELDS = %i(name active)

  belongs_to :organization
  has_and_belongs_to_many :app_groups
  has_and_belongs_to_many :nodes

  validates :name, presence: true, length: { minimum: 2, maximum: 50 }

  COLLECTIONS = %w(app_groups nodes)
  COLLECTION_ATTRIBUTES = %w(name id)
  
  include Collectable
end
