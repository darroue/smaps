# == Schema Information
# Schema version: 20220107222419
#
# Table name: settings
#
# @!attribute id
#   @return []
# @!attribute name
#   @return [String]
# @!attribute object_type
#   @return [String]
# @!attribute value
#   @return [String]
# @!attribute object_id
#   @return [Integer]
#
class Setting < ApplicationRecord
end
