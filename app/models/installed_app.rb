# == Schema Information
# Schema version: 20210804192444
#
# Table name: installed_apps
#
# @!attribute id
#   @return []
# @!attribute app_type
#   @return [String]
# @!attribute name
#   @return [String]
# @!attribute uninstall_string
#   @return [String]
# @!attribute version
#   @return [String]
# @!attribute created_at
#   @return [Time]
# @!attribute updated_at
#   @return [Time]
# @!attribute node_id
#   @return []
# @!attribute organization_id
#   @return []
#
# Indexes
#
#  index_installed_apps_on_node_id          (node_id)
#  index_installed_apps_on_organization_id  (organization_id)
#
class InstalledApp < ApplicationRecord
  belongs_to :organization
  belongs_to :node

  validates :version, :app_type, presence: true, length: { minimum: 2, maximum: 50 }
  validates :name, presence: true, length: { minimum: 2, maximum: 100 }
  validates :node, :organization, presence: true
  validates_uniqueness_of :name, scope: [:node, :version]
  validates_uniqueness_of :version, scope: [:node, :name]

  def can_be_uninstalled?
    app_type == "chocolatey"
    # || uninstall_string&.match(/msiexec/i) ? true : false
  end
end
