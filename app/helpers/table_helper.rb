module TableHelper
  def table_columns(field_names)
    field_names.map do |key|
      {
        field: key,
        title: key.to_s.split('.').join("\s").humanize,
        sortable: true
      }
    end
  end
end
