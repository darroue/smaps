module PermissionHelper
  PERMISSION_PAIRS = {
      organizations: {
        show: %w(show_organization_details),
        edit: %w(rename_organization),
        update: %w(rename_organization),
        destroy: %w(delete_organization)
      },
      apps: {
        index: %w(show_app_list),
        new: %w(create_app),
        show: %w(show_app_details),
        edit: %w(edit_app),
        update: %w(edit_app),
        create: %w(create_app),
        destroy: %w(delete_app)
      },
      roles: {
        index: %w(show_role_list),
        new: %w(create_role),
        show: %w(show_role_details),
        edit: %w(edit_role edit_role__users),
        update: %w(edit_role edit_role__users),
        create: %w(create_role),
        destroy: %w(delete_role)
      },
      app_groups: {
        index: %w(show_app_group_list),
        new: %w(create_app_group),
        show: %w(show_app_group_details),
        edit: %w(edit_app_group edit_app_group__apps),
        update: %w(edit_app_group edit_app_group__apps),
        create: %w(create_app_group),
        destroy: %w(delete_app_group)
      },
      nodes: {
        index: %w(show_node_list),
        new: %w(create_node),
        show: %w(show_node_details),
        edit: %w(edit_node edit_node__apps),
        update: %w(edit_node edit_node__apps),
        create: %w(create_node),
        destroy: %w(delete_node)
      },
      logs: {
        index: %w(show_log_list),
        show: %w(show_log_details),
        destroy: %w(delete_log),
        delete_all: %w(delete_log),
        delete_older_logs: %w(delete_log),
        keep: %w(delete_log),
      },
      node_groups: {
        index: %w(show_node_group_list),
        new: %w(create_node_group),
        show: %w(show_node_group_details),
        edit: %w(edit_node_group edit_node_group__node_memberhip edit_node_group__app_group_memberhip),
        update: %w(edit_node_group edit_node_group__node_memberhip edit_node_group__app_group_memberhip),
        create: %w(create_node_group),
        destroy: %w(delete_node_group)
      },
      installed_apps: {
        uninstall: %w(edit_node edit_node_app_list)
      },
      invites: {
        index: %w(show_invite_list),
        new: %w(create_user_invite),
        create: %w(create_user_invite),
        show: %w(show_invite_details),
        edit: %w(edit_invite),
        update: %w(edit_invite),
        renew: %w(edit_invite),
        destroy: %w(delete_invite)
      }
    }
end