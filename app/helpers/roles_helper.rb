module RolesHelper
  PROTECTED_ROLES = ['Owner', 'New Users']
  PERMISSIONS = {
    organization: %w(
      show_organization_details
      rename_organization
      delete_organization
    ),
    roles: %w(
      show_role_list
      show_role_details
      edit_role
      edit_role__users
      create_role
      delete_role
    ),
    apps: %w(
      show_app_list
      show_app_details
      create_app
      edit_app
      delete_app
    ),
    app_groups: %w(
      show_app_group_list
      show_app_group_details
      create_app_group
      edit_app_group
      edit_app_group__apps
      delete_app_group
    ),
    nodes: %w(
      show_node_list
      show_node_details
      create_node
      edit_node
      edit_node__apps
      delete_node
    ),
    logs: %w(
      show_log_list
      show_log_details
      delete_log
    ),
    node_groups: %w(
      show_node_group_list
      show_node_group_details
      create_node_group
      edit_node_group
      edit_node_group__nodes
      edit_node_group__app_groups
      delete_node_group
    ),
    invites: %w(
      show_invite_list
      show_invite_details
      create_user_invite
      edit_invite
      delete_invite
    )
  }
  ALL_PERMISSIONS = PERMISSIONS.values.flatten.compact
  OWNER_PERMISSIONS = ALL_PERMISSIONS
  NEW_USERS_PERMISSIONS = %w(show_role_list show_app_list show_app_group_list show_node_list show_node_group_list show_invite_list)
end
