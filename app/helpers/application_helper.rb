require_relative 'table_helper'
require_relative 'permission_helper'

module ApplicationHelper
  include TableHelper
  include PermissionHelper

  def multiple
    controller_name
  end

  def one
    multiple.singularize
  end

  def get_actions(actions, object, type = nil)
    return unless actions

    actions -= %i(edit destroy) if @noneditable

    actions.map do |action|
      label = action.to_s.humanize
      icon_class =  case action
                    when :edit
                      'fas fa-pencil-alt'
                    when :destroy, :uninstall_app, :remove_uninstall_app
                      'fa fa-trash text-danger'
                    when :index, :manage
                      'fas fa-list'
                    when :install
                      'fas fa-download'
                    when :accept
                      'fas fa-check-circle text-success'
                    when :reject
                      'fas fa-ban text-danger'
                    else
                      'fas fa-book-open'
                    end

      path =  case action
              when :show
                if multiple == 'organizations'
                  send(:organization_path,
                       { id: object })
                else
                  send(:"organization_#{one}_path", { id: object })
                end
              when :edit
                if multiple == 'organizations'
                  send(:edit_organization_path,
                       { id: object })
                else
                  send(:"edit_organization_#{one}_path",
                       { id: object })
                end
              when :destroy
                [
                  if multiple == 'organizations'
                    send(:organization_path,
                         { id: object })
                  else
                    send(:"organization_#{one}_path", { id: object })
                  end, { method: :delete, data: { confirm: 'Are you sure?' } }
                ]
              when :index
                label = :back.to_s.humanize

                multiple == 'organizations' ? send(:organizations_path) : send(:"organization_#{multiple}_path")
              else
                @custom_actions[action].call(object)
              end

      link_to(*path) do
        if type == :icons
          content_tag(:i, '', class: "#{icon_class} ml-2", title: label)
        elsif type == :text
          label
        else
          clazz = if action == :destroy
                    'danger'
                  elsif action != :index
                    'primary'
                  else
                    'secondary'
                  end

          content_tag(:div, label, class: "btn btn-#{clazz} ml-#{action == :destroy ? 5 : 2}")
        end
      end
    end.join(' ').html_safe
  end

  def clean_data
    # trim whitespace from beginning and end of string attributes
    attribute_names.each do |name|
      send("#{name}=", send(name).strip) if send(name).respond_to?(:strip)
    end
  end

  def find_from_param(param_name, id_attr = false)
    class_name = id_attr ? param_name.to_s.gsub(/_id(?=[^_id]*$)/, '').classify : param_name.to_s.classify
    clazz = Object.const_get(class_name)
    param = params[param_name]

    if param.present?
      id = case param
           when String
             param
           when Array
             param.reject(&:empty?)
           end
    end

    where_select = if @organization && clazz.has_attribute?(:organization)
                     { id: id,
                       organization: @organization }
                   else
                     { id: id }
                   end
    id_attr ? clazz.find(id) : clazz.where(**where_select)
  end

  def is_readonly?(readonly)
    @is_readonly ||= (readonly || @readonly)
  end

  def additional_class(readonly)
    readonly ? 'disabled' : ''
  end

  def input_text(form, name, readonly = false)
    concat(form.label(name, class: 'form-control-label'))
    concat(
      form.send(
        :text_field,
        name,
        class: "form-control form-control-user #{additional_class(readonly)}",
        readonly: is_readonly?(readonly),
        disabled: is_readonly?(readonly)
      )
    )
  end

  def input_datetime(form, name, readonly = false)
    concat(form.label(name, class: 'form-control-label'))
    concat(
      form.send(
        :datetime_field,
        name,
        class: "form-control form-control-user #{additional_class(readonly)}",
        readonly: is_readonly?(readonly),
        disabled: is_readonly?(readonly)
      )
    )
  end

  def input_number(form, name, readonly = false)
    concat(form.label(name, class: 'form-control-label'))
    concat(
      form.send(
        :number_field,
        name,
        class: "form-control form-control-user #{additional_class(readonly)}",
        readonly: is_readonly?(readonly),
        disabled: is_readonly?(readonly)
      )
    )
  end

  def input_email(form, name, readonly = false)
    concat(form.label(name, class: 'form-control-label'))
    concat(
      form.send(
        :email_field,
        name,
        class: "form-control form-control-user #{additional_class(readonly)}",
        readonly: is_readonly?(readonly),
        disabled: is_readonly?(readonly)
      )
    )
  end

  def input_textarea(form, name, readonly = false)
    concat(form.label(name, class: 'form-control-label'))
    concat(
      form.send(
        :text_area,
        name,
        rows: 10,
        class: "form-control form-control-user #{additional_class(readonly)}",
        readonly: is_readonly?(readonly),
        disabled: is_readonly?(readonly)
      )
    )
  end

  def entity_link(entity, field = :display_name, clazz = '')
    label = entity.send(field)
    path = :"organization_#{entity.class.name.underscore}_path"

    return label unless respond_to?(path)

    link_to(entity.send(field), send(path, { id: entity }), class: clazz)
  end

  def input_link(_, name, _readonly = false)
    concat(label_tag(name, nil, class: 'form-control-label'))
    concat(entity_link(@model_instance.send(name), :display_name, 'form-control form-control-user'))
  end

  def input_checkbox(form, name, readonly = false)
    concat(
      form.send(
        :check_box,
        name,
        class: "form-check-input #{additional_class(readonly)}",
        readonly: is_readonly?(readonly),
        disabled: is_readonly?(readonly)
      )
    )
    concat(form.label(name, class: 'form-check-label'))
  end

  def input_select(name, collection_options, readonly = false)
    concat(label_tag(name, nil, class: 'form-control-label'))
    concat(
      select_tag(
        name,
        collection_options,
        class: "form-control form-control-user #{additional_class(readonly)}",
        readonly: is_readonly?(readonly),
        disabled: is_readonly?(readonly),
        include_blank: true,
        multiple: true
      )
    )
  end
end
