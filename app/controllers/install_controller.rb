class InstallController < ApplicationController
  layout 'simple'

  before_action :install_params, :set_invite, only: [:show]
  skip_before_action :authenticate_user!, :get_organization, :default_params, :check_user_organizations, :check_user_permissions, only: [:show]

  def show
    status = if !@invite
      flash[:error] = 'Invite not found!'

      :not_found
    elsif !@invite.usable?
      flash[:error] = 'This invite is no longer usable!'

      :gone
    else
      :ok
    end

    respond_to do |format|
      format.ps1 { render :show }
      format.html { render :show, status: status }
    end
  end

  private

  def install_params
    params.permit(:auth_id)
  end

  def set_invite
    @invite = Invite.find_by(install_params)
  end
end
