class AppsController < CrudController
  def index
    super do
      @fields = %i(
        name
        package_name
        version!
        forced
        active
        actions
      )
    end
  end

  def destroy
    unless @app.app_groups.any?
      super
    else
      respond_to do |format|
        format.html { redirect_to organization_apps_url, flash: { error: "Can't destroy app that is part of App Group(s)!\n#{@app.app_groups.pluck(:name)}" } }
      end
    end
  end

  private

  def set_fields
    @fields = {
      name: nil,
      package_name: nil,
      version: nil,
      forced: :checkbox,
      active: :checkbox,
      nodes: :collection
    }
  end
end
