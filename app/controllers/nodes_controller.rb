class NodesController < CrudController
  def index
    super do
      @fields = %i(
        friendly_name
        hostname
        last_connected_at
        uninstall
        debug_mode
        active
        actions
      )
      @actions = %i(install) + @actions
      @custom_actions = {
        install: ->(node) {
          install_path(node.invite.auth_id)
        }
      }
    end
  end

  def show
    @custom_actions = {
      uninstall_app: ->(app) {
        [
          organization_node_uninstall_apps_path(
            @organization,
            node_id: @node.id,
            id: app.id
          ),
          { method: :post }
        ]
      },
      remove_uninstall_app: ->(app) {
        [
          organization_node_uninstall_app_path(
            @organization,
            node_id: @node.id,
            id: app.id
          ),
          { method: :delete }
        ]
      }
    }

    @app_groups = {}
    @node_installed_apps = @node.installed_apps
    @node.node_groups.active?.map(&:app_groups).flatten.each do |app_group|
      if app_group.active?
        @app_groups[app_group.name] = app_group.apps.order(name: :asc) if app_group.apps.any?
      else
        @app_groups[app_group.name] = false
      end
    end

    @app_groups[:"Node specific"] = @node.apps if @node.apps.any?

    # TODO

    @installed_apps = @node.installed_apps
    @uninstall_apps = @node.uninstall_apps

    super
  end

  private

  def set_fields
    @fields = {
      friendly_name: nil,
      active: :checkbox,
      uninstall: :checkbox,
      debug_mode: :checkbox,
      server_url: nil,
      schedule: nil,
      node_groups: :collection,
      apps: :collection
    }
  end
end
