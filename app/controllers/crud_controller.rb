class CrudController < ApplicationController
  before_action only: %i(show edit update destroy) do
    instance_params = { id: params[:id] }
    instance_params[:organization] = @organization if @organization

    instance = model.find_by(**instance_params)

    head :not_found unless instance

    set_model_instance(instance)
  end

  before_action :set_fields, except: %i(index destroy)

  def index
    @collection = model.where(organization: @organization)
    @actions = []

    yield if block_given?
  end

  def show
    @readonly = true
    @actions = %i(edit index)
  end

  def new
    set_model_instance(model.new)
  end

  def edit
    @actions = %i(show index destroy)

    yield if block_given?
  end

  def create
    set_model_instance(model.new(model_params))

    respond_to do |format|
      set_collections

      if !@model_instance.errors.present? && @model_instance.save

        yield if block_given?

        format.html do
          redirect_to(
            {
              controller: controller_name,
              action: :show,
              id: @model_instance.id
            },
            flash: {
              success: "#{instance_name_s} was successfully created."
            }
          )
        end
        format.json { render :show, status: :created, location: @model_instance }
      else
        flash[:error] = @model_instance.errors.full_messages.join('<br/>').html_safe
        format.html { render :new }
        format.json { render json: @model_instance.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      set_collections
      @model_instance.assign_attributes(model_params)

      if !@model_instance.errors.present? && @model_instance.save
        yield if block_given?

        format.html do
          redirect_to(
            {
              controller: controller_name,
              action: :show,
              id: @model_instance.id
            },
            flash: {
              success: "#{instance_name_s} was successfully updated."
            }
          )
        end
        format.json { render :show, status: :ok, location: @model_instance }
      else
        flash[:error] = @model_instance.errors.full_messages.join('<br/>').html_safe
        format.html { render :edit, status: :ok }
        format.json { render json: @model_instance.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @model_instance.destroy
        yield if block_given?

        format.html do
          redirect_to(
            {
              controller: controller_name,
              action: :index
            },
            flash: {
              success: "#{instance_name_s} was successfully destroyed."
            }
          )
        end
      else
        format.html do
          redirect_to(
            {
              controller: controller_name,
              action: :index
            },
            flash: {
              error: @model_instance.errors.full_messages
            }
          )
        end
      end
    end
  end

  private

  def set_model_instance(value, plural = false)
    @model_instance ||= value
    instance_variable_set(
      "@#{plural ? instance_name_plural : instance_name}",
      value
    )
  end

  def model_params
    @model_params ||= if params.key?(model_name)
                        permitted_fields = model::PERMITED_FIELDS
                        keys, pairs = [], {}

                        if permitted_fields.is_a?(Array)
                          keys = permitted_fields
                        else
                          permitted_fields.each_pair { |k, v| v.nil? ? keys.push(k) : pairs[k] = v }
                        end

                        @default_params.merge(params.require(model_name).permit(*keys, **pairs))
                      else
                        {}
                      end
  end

  %i(apps nodes node_groups app_groups users).each do |type|
    define_method(:"set_#{type}") do
      if @model_instance.respond_to?(:"#{type}_validation")
        set_collection(type) if @model_instance.send(:"#{type}_validation", find_from_param(type))
      else
        set_collection(type)
      end
    end
  end

  def set_collection(type)
    @model_instance.send(:"#{type}=", find_from_param(type))
  end

  def set_collections
    set_fields.to_a.each do |key, value|
      if value == :collection
        send(:"set_#{key}") # if has_permission_to(:"set_#{key}")
      end
    end
  end
end
