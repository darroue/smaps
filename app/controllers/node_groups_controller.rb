class NodeGroupsController < CrudController
  before_action :available_nodes, :available_app_groups

  def index
    super do
      @fields = %i(
        name
        active
      )
    end
  end

  private

  def available_app_groups
    @available_app_groups = AppGroup.where(organization: @organization)
  end

  def set_app_groups
    @node_group.app_groups = find_from_param :app_groups
  end

  def available_nodes
    @available_nodes = Node.where(organization: @organization)
  end

  def set_nodes
    @node_group.nodes = find_from_param :nodes
  end

  def set_fields
    @fields = {
      name: nil,
      active: :checkbox,
      app_groups: :collection,
      nodes: :collection
    }
  end
end
