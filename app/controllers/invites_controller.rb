class InvitesController < CrudController
  before_action only: %i(renew accept reject) do
    set_model_instance(model.find(params[:id]))
  end
  before_action only: %i(accept reject) do
    invalid_user_permissions unless @invite.user == current_user
  end
  skip_before_action :get_organization,
                     :default_params,
                     :check_user_organizations,
                     :check_user_permissions,
                     only: %i(accept reject)

  def index
    super do
      @collection = @organization.invites.includes(:node, :user).where(node_id: nil)
      @fields = %i(
        identificator
        invite_valid_until
        remaining_usages
      )
    end
  end

  def accept
    unless @invite.usable?
      redirect_to organizations_path, flash: { error: 'Invite no longer usable!' }
      return
    end

    organization = @invite.organization
    organization_roles = organization.roles.find_by(name: 'New Users')

    current_user.transaction do
      current_user.organizations << organization unless organization.in?(current_user.organizations)
      current_user.roles << organization_roles unless organization_roles.in?(current_user.roles)

      respond_to do |format|
        if current_user.errors.present?
          format.html { redirect_to organizations_path }
        else
          @invite.destroy
          format.html do
            redirect_to(
              organization_apps_path(organization),
              flash: {
                info: "Welcome to #{organization.name}"
              }
            )
          end
        end
      end
    end
  end

  def reject
    if @invite.destroy
      respond_to do |format|
        format.html do
          redirect_to(
            organizations_path,
            flash: {
              success: 'Invite was successfully rejected.'
            }
          )
        end
        format.json { head :success }
      end
    end
  end

  def set_fields
    @fields = {
      invite_type: :collection,
      email: :email,
      usages: :number,
      valid_until: :datetime,
      auth_id: :readonly,
      node_install_url: :readonly
    }
  end

  def set_invite_type
    @model_instance.send(:"invite_type=", params[:invite_type]&.first)
  end
end
