class Api::V1::NodesController < ApplicationController
  before_action :set_api_v1_node, only: [:show, :update, :log]
  before_action :set_api_v1_node_invite, only: [:register]
  skip_before_action :authenticate_user!, :get_organization, :default_params, :check_user_organizations, :check_user_permissions

  respond_to :json

  def show
    if @api_v1_node.active?
      attributes = %i(package_name version forced)
      @api_v1_node.update last_connected: DateTime.now

      # Node Apps
      apps_to_uninstall = @api_v1_node.uninstall_apps.map { |app|
        keys = %i(name version app_type)
        if app.app_type == "other"
          next unless app.try(:[], :uninstall_string).match(/msiexec/i)
          app.uninstall_string = app.uninstall_string.match(/\{(((\d|\w)+-?){5,6})\}/).captures.first.to_s
          keys << :uninstall_string
        end

        app_attrs = app.attributes.symbolize_keys.slice(*keys)
      }.compact

      apps_to_uninstall_package_names = apps_to_uninstall.collect { |app| app[:name] }
      apps_to_install = (@api_v1_node.apps.active? + App.active?.joins(app_groups: [{ node_groups: :nodes }]).where(
        app_groups: { active: true },
        node_groups: { active: true },
        nodes: { id: @api_v1_node.id }
      ).reverse).select { |app| !app.package_name.in?(apps_to_uninstall_package_names) }

      # Output formating
      apps_to_install = apps_to_install.uniq { |app| app[:package_name] }.sort_by { |app| app[:name] }.map do |app|
        app_version = app.version!
        app = app.slice(*attributes)
        app[:version] = app_version
        app
      end

      @data = {
        apps_to_install: apps_to_install,
        apps_to_uninstall: apps_to_uninstall,
        uninstall: @api_v1_node.uninstall,
        debug_mode: @api_v1_node.debug_mode,
        server_url: @api_v1_node.server_url || @api_v1_node.organization.server_url,
        schedule: schedule
      }

      respond_with(@data)
    else
      head :no_content
    end
  end

  def schedule
    setting = @api_v1_node.schedule || @api_v1_node.organization.schedule

    if setting.present?
      setting.split(',')
    end
  end

  def update
    # Update hostname and last_connected
    @api_v1_node.update(hostname: api_v1_node_params[:hostname], last_connected: DateTime.now)

    # Save data to variables
    needed_attributes = %w(name version app_type)
    server_side_apps = @api_v1_node.installed_apps
    server_side_apps_h = server_side_apps.pluck_to_hash(*needed_attributes)
    server_side_uninstall_apps = @api_v1_node.uninstall_apps
    server_side_uninstall_apps_h = server_side_uninstall_apps.pluck_to_hash(*needed_attributes)

    client_side_apps = api_v1_node_params[:installed_packages].map(&:to_h)
    client_side_apps_h = client_side_apps.map { |h| h.slice(*needed_attributes) }

    # Select apps to add and remove
    apps_to_store_on_server = client_side_apps_h.reject { |app| server_side_apps_h.include?(app) }
    apps_to_remove_from_server = server_side_apps_h.reject { |app| client_side_apps_h.include?(app) }
    uninstall_apps_to_remove_from_server = server_side_uninstall_apps_h.reject { |app| client_side_apps_h.include?(app) }

    # Remove apps that are no longer installed on node
    server_side_apps.each { |app| app.delete if apps_to_remove_from_server.include?(app.attributes.slice(*needed_attributes)) }
    server_side_uninstall_apps.each { |app| app.delete if uninstall_apps_to_remove_from_server.include?(app.attributes.slice(*needed_attributes)) }

    # Add newly installed apps
    apps_to_store_on_server.each { |app| server_side_apps.find_or_create_by!(app.merge({ organization: @api_v1_node.organization })) }
  end

  def register
    unless @api_v1_node_invite.usable?
      head :not_acceptable
    else
      node = @api_v1_node_invite.node || @api_v1_node_invite.organization.nodes.new(friendly_name: api_v1_node_register_params[:hostname])
      unless node.auth_id
        if node.update(api_v1_node_register_params.merge({ last_connected: DateTime.now, auth_id: Digest::SHA256.hexdigest(Time.now.to_s) }))
          @api_v1_node_invite.use
          render json: { message: "Node succesfully registered!" }.merge(node.slice(:auth_id))
        else
          head :internal_server_error
        end
      else
        render json: { message: "Node already registered!" }, status: :found
      end
    end
  end

  def log
    node = @api_v1_node
    
    # Keep only latest log for node per day (Logs are incremental)
    node.logs.for_today.delete_all
    node.logs.create(content: params[:log], organization: node.organization)
    # Disable debug mode if active
    node.update(debug_mode: false) if node.debug_mode

    head :ok
  end

  private

  def set_api_v1_node
    @api_v1_node = Node.find_by_auth_id(api_v1_node_params[:auth_id])
    head :forbidden unless @api_v1_node
  end

  def set_api_v1_node_invite
    @api_v1_node_invite = Invite.find_by_auth_id(params[:invite_auth])
    head :forbidden unless @api_v1_node_invite
  end

  def api_v1_node_params
    params.permit(:auth_id, :hostname, installed_packages: [:name, :version, :app_type, :uninstall_string])
  end

  def api_v1_node_register_params
    params.permit(:hostname, :guid)
  end
end
