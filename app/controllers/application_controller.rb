class ApplicationController < ActionController::Base
  include ::ApplicationHelper
  include ::RolesHelper

  before_action :authenticate_user!, :get_organization, :default_params, :check_user_organizations
  before_action :check_user_permissions, unless: [-> { request.path == root_path }]
  before_action :configure_permitted_parameters, if: :devise_controller?
  skip_before_action :get_organization, :default_params, :check_user_organizations, :check_user_permissions, if: :devise_controller?
  protect_from_forgery unless: -> { request.format.json? }

  def check_user_organizations
    if params[:organization_id]
      unless @organization.in?(current_user.organizations)
        redirect_to root_path, flash: { error: "You dont have access to organization with ID: #{params[:organization_id]}!" }
      end
    end
  end

  def check_user_permissions
    required_permissions = PERMISSION_PAIRS[controller_name.to_sym].try(:[], action_name.to_sym) || []
    unless (required_permissions & current_user.permissions(@organization)).present?
      invalid_user_permissions
    end
  end

  def has_permission_to(child_permission)
    permission = action_name
    permission = "edit" if permission.in?(%w(create update))
    child_permission = child_permission.to_s.gsub(/^set_/, "")

    current_user.permissions(@organization).include?("#{permission}_#{instance_name}__#{child_permission}")
  end

  def invalid_user_permissions
    # TODO: Add waterfall Destroy/Update -> Show -> Index -> Apps -> Root based on permissions
    redirect_to ('show_app_list'.in?(current_user.permissions(@organization)) ? organization_apps_path(@organization) : root_path), flash: { error: 'You dont have access to that action!' }
  end

  def default_params
    @default_params = if @organization
      { organization_id: @organization.id }
    else
      {}
    end
  end

  def get_organization
    @organization = find_from_param :organization_id, true
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path, flash: { error: "Organization with ID #{params[:organization_id]} were not found!" }
  end

  def model_name
    @model_name ||= controller_name.singularize.to_sym
  end

  def instance_name
    model_name.to_s
  end

  def model
    @model ||= Object.const_get(instance_name.classify)
  end

  def instance_name_s
    instance_name.titleize
  end

  def instance_name_plural
    instance_name.pluralize
  end

  protected

  def configure_permitted_parameters
    permitted_parameters = %i(first_name last_name email password password_confirmation)

    devise_parameter_sanitizer.permit(:sign_up) do |user_params|
      user_params.permit(*permitted_parameters)
    end

    devise_parameter_sanitizer.permit(:account_update) do |user_params|
      user_params.permit(*permitted_parameters, :current_password)
    end
  end
end
