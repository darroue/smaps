class AppGroupsController < CrudController
  def index
    super do
      @fields = %i(
        name
        active
      )
    end
  end

  private

  def set_fields
    @fields = {
      name: nil,
      active: :checkbox,
      apps: :collection,
      node_groups: :collection
    }
  end
end
