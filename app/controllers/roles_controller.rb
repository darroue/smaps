class RolesController < CrudController
  before_action :set_noneditable, only: %i(show edit)

  def set_noneditable
    @noneditable = true if @model_instance.protected?
  end

  def index
    super do
      @fields = %i(
        name
      )
    end
  end

  private

  def set_fields
    @fields = {
      name: nil,
      users: :collection,
      permissions: :collection
    }
  end

  def set_permissions
    @model_instance.send(:"permissions=", params[:permissions])
  end
end
