class OrganizationsController < CrudController
  include ::RolesHelper
  skip_before_action :get_organization
  before_action :check_user_permissions, except: %i(new create index)

  def index
    super do
      @collection = current_user.user_organizations
      @fields = %i(
        name
        actions
      )
      @actions = %i(show)
      @custom_actions = {
        manage: ->(organization) {
          organization_apps_path(organization)
        },
        show_organization: ->(organization) {
          organization
        },
        accept: ->(invite) {
          accept_organization_invite_path(invite.organization, invite)
        },
        reject: ->(invite) {
          reject_organization_invite_path(invite.organization, invite)
        }
      }
      @pending_invites = current_user.invites.usable
    end
  end

  private

  def set_fields
    @fields = {
      name: nil,
      current_user_id: nil,
      users: :collection,
      server_url: nil,
      schedule: nil
    }
  end

  def set_collection(type)
    if type == :users
      users_before = @model_instance.users
      users_after = find_from_param(type)
      removed_users = users_before - users_after

      @model_instance.roles.each do |role|
        remove_users_from_role(removed_users, role)
      end

      @model_instance.send(:"#{type}=", find_from_param(type))
    end
  end

  def remove_users_from_role(removed_users, role)
    role.users = role.users - removed_users
  end
end
