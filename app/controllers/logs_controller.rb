class LogsController < CrudController
  before_action :set_noneditable, only: %i(show edit)

  def set_noneditable
    @noneditable = true
  end

  def index
    @collection = model.where(organization: @organization).includes(:loggable)
    @fields = %i(
      entity
      log_created_at
      severity
      actions
    )
    @actions = [:destroy]
  end

  def delete_all
    delete_multiple(logs)
  end

  def delete_older_logs
    delete_multiple(logs.older_than(params[:age].to_i))
  end

  def keep
    delete_multiple(logs.keep(params[:amount].to_i))
  end

  def logs
    if node?
      @organization.nodes.find(params[:node_id]).logs
    else
      @organization.logs
    end
  end

  def node?
    params[:node_id].present?
  end

  def redirect_path
    if node?
      organization_node_path(organization_id: @organization.id, id: params[:node_id])
    else
      organization_logs_path
    end
  end

  def delete_multiple(logs)
    respond_to do |format|
      if logs.delete_all
        format.html do
          redirect_to(
            redirect_path,
            flash: {
              success: 'Logs were successfully destroyed.'
            }
          )
        end
      else
        format.html do
          redirect_to(
            redirect_path,
            flash: {
              error: 'There was a error while destroing logs'
            }
          )
        end
      end
    end
  end

  def set_fields
    @fields = {
      entity: :link,
      created_at: :datetime,
      severity: nil,
      content: :textarea
    }
  end
end
