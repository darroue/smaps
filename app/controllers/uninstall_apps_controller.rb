class UninstallAppsController < CrudController
  skip_before_action :check_user_permissions, only: %i(create destroy) # TODO
  before_action :set_node, :set_app
  before_action :check_if_app_can_be_uninstalled?, only: %i(create)

  def create
    super do
      redirect_to(organization_node_path(@organization, @node), flash: { success: "<p>App <strong>#{@model_instance.name}</strong> were successfully marked for uninstall.</p>" })
      return
    end
  end

  def destroy
    super do
      redirect_to(organization_node_path(@organization, @node), flash: { success: "<p>App <strong>#{@model_instance.name}</strong> is no longer marked for uninstall." })
      return
    end
  end

  def model_params
    @app.attributes.except(*%w(created_at updated_at id)).symbolize_keys
  end

  def set_fields
  end

  def set_node
    @node = @organization.nodes.find(params[:node_id])
  end

  def set_app
    @app = @node.installed_apps.find_by(id: params[:id])
  end

  def check_if_app_can_be_uninstalled?
    unless @app.can_be_uninstalled?
      redirect_to(organization_node_path(@organization, @node), flash: { error: "<p>App <strong>#{@app.name}</strong> can't be marked for uninstall at this moment!" })
    end
  end
end
